//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//Lisa Fr�hlich Gabra, #766765, Project 5: Expanding the Garden of Knowledge  //
//Growing TogethAR                                                            //
//Script for all the information during the game                              //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


//last changes: 12.02.2022


//What it do:
// - stores the riddle objects and their indicators
// - stores the information to place the riddle objects and the seed
// - stores the games state


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class StateHandler : MonoBehaviour
{
    public bool placed; //has the riddle object been placed
    public int plantState = 0; //in what state is the riddle object currently in

    //storing game objects for later use
    public GameObject plant1; //ref to the first riddle object
    public GameObject plant2; //ref to the second riddle object
    public GameObject plant3; //ref to the third riddle object

    public GameObject indicator1; //ref to the indicator for the first riddle object
    public GameObject indicator2; //ref to the indicator for the second riddle object
    public GameObject indicator3; //ref to the indicator of the third riddle object

    public GameObject seed; //ref to the seed object

    //storing AR relevant information
    public ARRaycastManager raycastManager; //ref to the raycast manager on the AR session origin object
    public ARSessionOrigin arOrigin; //ref to the origin of this AR session

    //variables for placement
    public bool poseIsValid = false; //does the camera look at a point where the riddle object can be placed (horizontal plane)
    public Pose placementPose; //ref to the position the camera is pointing to

    //variables for the blossom riddle
    public int blossomCount = 1; //the counter for the times the user has tapped a right blossom

    // Start is called before the first frame update
    void Start()
    {
        raycastManager = FindObjectOfType<ARRaycastManager>(); //storing the AR raycast manager in the beginning of the session
        arOrigin = FindObjectOfType<ARSessionOrigin>(); //storing the AR session origin in the beginning of the session

        //putting the existing models in their first state
        indicator1.SetActive(false);
        indicator2.SetActive(false);
        indicator3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
