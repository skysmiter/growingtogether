//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//Lisa Fr�hlich Gabra, #766765, Project 5: Expanding the Garden of Knowledge  //
//Growing TogethAR                                                            //
//Script to handle the animations of the riddle objects                       //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


//last changes: 12.02.2022


//What it do:
// - 


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animations : MonoBehaviour
{
    private StateHandler states; //ref to the script that stores the riddles states
    private Animator anim;

    [Header("Input these booleans name into all the animators that include it!")]
    [SerializeField] string bool1, bool2, bool3;

    // Start is called before the first frame update
    void Start()
    {
        states = GetComponent<StateHandler>(); //get the script from the script handler
    }

    // Update is called once per frame
    void Update()
    {
        anim = GameObject.FindGameObjectWithTag("Plant").GetComponent<Animator>(); //find the current riddle object and use their animator

        //depending on the plant state set the correct boolean to true (applies to all animators due to naming being the same everywhere)
        if (states.plantState == 1 && !anim.GetBool(bool1))
        {
            anim.SetBool(bool1, true);
        }
        else if (states.plantState == 2 && !anim.GetBool(bool2))
        {
            anim.SetBool(bool2, true);
        }
        else if (states.plantState == 3 && !anim.GetBool(bool3))
        {
            anim.SetBool(bool3, true);
        }
    }
}
