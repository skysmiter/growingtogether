//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//Lisa Fr�hlich Gabra, #766765, Project 5: Expanding the Garden of Knowledge  //
//Growing TogethAR                                                            //
//Script to update the placement pose                                         //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


//last changes: 08.02.2022


//What it do:
// - check if the placement pose is valid
// - update the placement pose according to the camera
// - only calculate when necessary


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

public class PlacementPose : MonoBehaviour
{
    private StateHandler states; //ref to the script that stores the riddles states

    // Start is called before the first frame update
    void Start()
    {
        states = GetComponent<StateHandler>(); //get the script from the script handler
    }

    // Update is called once per frame
    void Update()
    {
        //calculate the placement position if it is necessary
        if (states.placed == false) //if the riddle object is not placed
        {
            CheckPose(); //calculate the position for the indicator and check if it is valid
        }
        else states.poseIsValid = false; //the placementPose is always invalid if the riddle object is placed
    }

    //Check Pose gets the pose from the camera and checks if it is valid
    private void CheckPose()
    {
        var screenCenter = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f)); //middle of the screen
        var hits = new List<ARRaycastHit>(); //empty list of objects hit by the ray
        states.raycastManager.Raycast(screenCenter, hits, TrackableType.Planes); //raycast from the AR raycast manager/session origin to the point the camera is looking at

        states.poseIsValid = hits.Count > 0; //the pose is valid if the camera is pointing at a plane

        if (states.poseIsValid) //if the placement pose is valid
        {
            states.placementPose = hits[0].pose; //update the placement pose according to where the camera is looking to
        }
    }
}
