//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//Lisa Fr�hlich Gabra, #766765, Project 5: Expanding the Garden of Knowledge  //
//Growing TogethAR                                                            //
//Script to update the placement indicator                                    //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


//last changes: 08.02.2022


//What it do:
// - show the according indicator
// - update the indicators position according to the camera
// - hide the indicators if not needed
// - can differentiate between the two versions of the indicator and use them according to the game states


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicator : MonoBehaviour
{
    private StateHandler states; //ref to the script that stores the riddles states

    // Start is called before the first frame update
    void Start()
    {
        states = GetComponent<StateHandler>(); //get the script from the script handler
    }

    // Update is called once per frame
    void Update()
    {
        if (states.poseIsValid) //if the plant has not been placed and the camera is looking at a horizontal plane
        {
            if (states.plantState == 0) PlacementIndicator(states.indicator1); //if the game is in the first state display the first indicator
            else if (states.plantState == 1) PlacementIndicator(states.indicator2); //if the game is in the second state display the second indicator
            else if (states.plantState > 2) PlacementIndicator(states.indicator3); //if the game is in a higher state than the second one display the third indicator
        }
        else NoIndicator(); //if no object can be placed hide all indicators
    }

    //Placement Indicator sets the requested indicator object to active and updates its position according to the placement pose
    private void PlacementIndicator(GameObject indicator)
    {
        indicator.SetActive(true); //show the indicator for the according object
        indicator.transform.SetPositionAndRotation(states.placementPose.position, states.placementPose.rotation); //move and rotate the indicator according to the movement of the camera
    }


    //No Indicator sets all indicator objects from the scene to inactive
    private void NoIndicator()
    {
        states.indicator1.SetActive(false);
        states.indicator2.SetActive(false);
        states.indicator3.SetActive(false);
    }
}
