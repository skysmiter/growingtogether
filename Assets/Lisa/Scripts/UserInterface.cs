//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//Lisa Fr�hlich Gabra, #766765, Project 5: Expanding the Garden of Knowledge  //
//Growing TogethAR                                                            //
//Script to handle all the UI elements and their functions                    //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


//last changes: 12.02.2022


//What it do:
// - hide/show the replace button according to the games state
// - provides function for replace button
// - hide/show the scan indicator according to the game state
// - hide/show the open image button
// - provides function for the open image button as well as the close button on the scholar image
// - displays the eye icon whenever necessary
// - displays the according poison message
// - displays a chromatic aberration according to the poison levels of the user
// - hides the help and replace button during the display of the scholar image


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class UserInterface : MonoBehaviour
{
    //the UI elements
    public GameObject helpButton; //ref to the help button
    public GameObject replaceButton; //ref to the replace button to replace the riddle object if it bugs out of the plane tracking
    public GameObject scanRoomIndicator; //ref to the indicator for the user to scan the room
    public GameObject openImage; //ref to the button that shows the image for the scholar
    public GameObject eyeIcon; //ref to the icon that allows the user to look at their partners phone
    public GameObject victory; //ref to the text that displays victory
    public GameObject scholarImage; //ref ot the button/image that the explorer should show the scholar

    public GameObject postProcessing; //ref to the game object that handles the poison vignette
    private ChromaticAberration chromAb; //var to change the intensity of the chromatic aberration

    public TextMeshProUGUI poisonMessage; //ref to the text that tells the user they are poisoned
    [SerializeField]
    private string message1, message2, message3; //the messages for the user

    private StateHandler states; //ref to the script that stores the riddles states

    [SerializeField] GameObject hint1, hint2, hint3, hint4, hint5, hint6; //the hint fields for the help button

    [SerializeField] GameObject next1, next2; //ref to the buttons on the sides of the hints

    // Start is called before the first frame update
    void Start()
    {
        states = GetComponent<StateHandler>(); //get the script from the script handler

        //the UI elements beginning state
        helpButton.SetActive(true);
        replaceButton.SetActive(false);
        scanRoomIndicator.SetActive(false);
        openImage.SetActive(false);
        eyeIcon.SetActive(false);
        victory.SetActive(false);
        scholarImage.SetActive(false);

        postProcessing.GetComponent<Volume>().profile.TryGet(out chromAb); //get the intensity of the chromatic aberration

        poisonMessage.text = message1; //needs to be text mesh pro component

        hint1.SetActive(false);
        hint2.SetActive(false);
        hint3.SetActive(false);
        hint4.SetActive(false);
        hint5.SetActive(false);
        hint6.SetActive(false);
        hint6.SetActive(false);

        next1.SetActive(false);
        next2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //handle the replace button
        if (states.placed == false) replaceButton.SetActive(false); //if the riddle object has not been placed, hide the button
        else replaceButton.SetActive(true); //if the riddle object has been placed, show the button

        //handle the scan indicator
        if (!states.placed && !states.poseIsValid) scanRoomIndicator.SetActive(true); //if the riddle object has not been placed and the user is also not scanning valid pose options display the scan indicator
        else scanRoomIndicator.SetActive(false); //otherwise hide the scan indicator

        //handle the open image button
        if (states.plantState == 3 && !openImage.activeSelf) openImage.SetActive(true); //show the button the first time
        if (scholarImage.activeSelf && openImage.activeSelf) openImage.SetActive(false); //if the scholar image and the button are both open hide the open image button

        //handle the poison message
        if (states.plantState == 0 && poisonMessage.text != message1) poisonMessage.text = message1; //if the plant is still emmitting poison, display the first message
        else if (states.plantState > 0 && states.plantState < 5 && poisonMessage.text != message2) poisonMessage.text = message2; //if the user has not healed themselves yet, display the second message
        else if (states.plantState > 4 && poisonMessage.text != message3) poisonMessage.text = message3; //if the user has healed themselves, display the third message

        //handle the post processing
        if (states.plantState == 1 && chromAb.intensity.value != 0.5f) chromAb.intensity.value = 0.5f; //if the plant has been tickled and the chromatic aberration has not been tuned down, tune it down
        else if (states.plantState > 4 && chromAb.intensity.value != 0.0f) chromAb.intensity.value = 0.0f; //if the seed has been eaten and the chromatic aberration is not gone, turn it off

        //handle the victory message
        if (states.plantState > 4 && !victory.activeSelf) victory.SetActive(true);

        //handle the next buttons
        if ((hint1.activeSelf || hint3.activeSelf || hint5.activeSelf) && !next1.activeSelf) //if one of the odd hints is visible, but the according next button is not, make the button visible
        {
            next1.SetActive(true);
            next2.SetActive(false); //and hide the other button
        }
        else if ((hint2.activeSelf || hint4.activeSelf || hint6.activeSelf) && !next2.activeSelf) //if one of the even hints is visible, but the according next button is not, make the button visible
        {
            next2.SetActive(true);
            next1.SetActive(false); //and hide the other one
        }
    }

    //Function for the Replace Button: Destroy the riddle object and tell the riddle about it
    public void RemoveObject()
    {
        Destroy(GameObject.FindGameObjectWithTag("Plant"));
        states.placed = false;
    }

    //Function for the Open Image Button: Open the scholar image and hide yourself as well as the icons in the background
    public void OpenImage()
    {
        helpButton.SetActive(false);
        replaceButton.GetComponent<Image>().enabled = false;
        victory.GetComponent<TextMeshProUGUI>().enabled = false;
        poisonMessage.enabled = false;
        scholarImage.SetActive(true);
        eyeIcon.SetActive(true);
    }

    //Function for the Close Button on the Scholar Image: shows the open image button as well as the icons in the background and hides itself
    public void CloseImage()
    {
        openImage.SetActive(true);
        helpButton.SetActive(true);
        replaceButton.GetComponent<Image>().enabled = true;
        victory.GetComponent<TextMeshProUGUI>().enabled = true;
        poisonMessage.enabled = true;
        eyeIcon.SetActive(false);
        scholarImage.SetActive(false);
    }

    //Function for the Help Button: show or hide the hints according to the games state
    public void Hints()
    {
        if (states.plantState == 0) //if the plant has not been tickled yet
        {
            if (!hint1.activeSelf && !hint2.activeSelf) hint1.SetActive(true);
            else
            {
                //hide all hints
                hint1.SetActive(false);
                hint2.SetActive(false);
                hint3.SetActive(false);
                hint4.SetActive(false);
                hint5.SetActive(false);
                hint6.SetActive(false);
                next1.SetActive(false);
                next2.SetActive(false);
            }
        }
        else if (states.plantState == 1) //if the plant has been tickled but not tapped
        {
            if (!hint3.activeSelf && !hint4.activeSelf) hint3.SetActive(true);
            else
            {
                //hide all hints
                hint1.SetActive(false);
                hint2.SetActive(false);
                hint3.SetActive(false);
                hint4.SetActive(false);
                hint5.SetActive(false);
                hint6.SetActive(false);
                next1.SetActive(false);
                next2.SetActive(false);
            }
        }
        else if (states.plantState > 1 && states.plantState < 5) //if the plant has been tickled and tapped, but the seed has not been eaten
        {
            if (!hint5.activeSelf && !hint6.activeSelf) hint5.SetActive(true);
            else
            {
                //hide all hints
                hint1.SetActive(false);
                hint2.SetActive(false);
                hint3.SetActive(false);
                hint4.SetActive(false);
                hint5.SetActive(false);
                hint6.SetActive(false);
                next1.SetActive(false);
                next2.SetActive(false);
            }
        }
        else if (states.plantState > 4) //if the seed has been eaten and the user won
        {
            //hide all hints
            hint1.SetActive(false);
            hint2.SetActive(false);
            hint3.SetActive(false);
            hint4.SetActive(false);
            hint5.SetActive(false);
            hint6.SetActive(false);
            next1.SetActive(false);
            next2.SetActive(false);
        }
    }

    //Function for the Next Buttons: hides the according hints and displays the others
    public void NextButton(GameObject button)
    {
        if (button == next1)
        {
            if (states.plantState == 0) //if the plant has not been tickled yet
            {
                hint1.SetActive(false);
                hint2.SetActive(true);
            }
            else if (states.plantState == 1) //if the plant has been tickled but not tapped
            {
                hint3.SetActive(false);
                hint4.SetActive(true);
            }
            else if (states.plantState > 1 && states.plantState < 5) //if the plant has been tickled and tapped, but the seed has not been eaten
            {
                hint5.SetActive(false);
                hint6.SetActive(true);
            }
        }
        else if (button == next2)
        {
            if (states.plantState == 0) //if the plant has not been tickled yet
            {
                hint1.SetActive(true);
                hint2.SetActive(false);
            }
            else if (states.plantState == 1) //if the plant has been tickled but not tapped
            {
                hint3.SetActive(true);
                hint4.SetActive(false);
            }
            else if (states.plantState > 1 && states.plantState < 5) //if the plant has been tickled and tapped, but the seed has not been eaten
            {
                hint5.SetActive(true);
                hint6.SetActive(false);
            }
        }
    }
}
