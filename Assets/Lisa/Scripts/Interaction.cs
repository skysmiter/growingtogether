//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//Lisa Fr�hlich Gabra, #766765, Project 5: Expanding the Garden of Knowledge  //
//Growing TogethAR                                                            //
//Script to handle input from the user                                        //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


//last changes: 12.02.2022


//What it do:
// - check for input
// - check if and what was hit
// - place the riddle object if the empty screen was hit while the object is placable
// - store the colours for the tickle colour change
// - change the petals colour if the petals were hit until we go to the next riddle state
// - if we get to the second state, hide all spikes and show all blossoms
// - handles the tapping of the blossoms in the right order until we go to the next state
// - changes the blossoms material according to the blossom riddle
// - tap the seed to eat it and win
// - stop the particles from the plant


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interaction : MonoBehaviour
{
    //variables for tickleing
    public Material petalMat; //ref to the material of the petals
    public Color color1;
    public Color color2;
    public Color color3;
    public Color color4;
    public Color goal; //the color we want to have in the end
    private int tickleCount = 1; //the counter for the times one has to tickle

    //variables for tapping blossoms
    [SerializeField] string blossom1, blossom2, blossom3, blossom4; //the names the blossoms on the riddle objects should have
    [SerializeField] Material oldMat, newMat; //the materials the blossoms can be set to

    private StateHandler states; //ref to the script that stores the riddles states

    // Start is called before the first frame update
    void Start()
    {
        states = GetComponent<StateHandler>(); //get the script from the script handler
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) //interaction with screen
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position); //position of the screen the user tapped on
            RaycastHit hit; //ref to store an object that was hit by the ray

            if (!Physics.Raycast(ray, out hit)) //interaction with the empty screen
            {
                if (states.poseIsValid) //riddle object is not placed and user is looking at a horizontal plane
                {
                    if (states.plantState == 0) PlacePlant(states.plant1); //place the first riddle object when in the first game state
                    else if (states.plantState == 1) PlacePlant(states.plant2); //if the game is in the second state, place the second riddle object
                    else if (states.plantState > 2) PlacePlant(states.plant3); //if the game is in a higher state than the second, place the third riddle object
                }
            }
            else if (Physics.Raycast(ray, out hit)) //interaction with an object
            {
                if (hit.collider.transform.CompareTag("Petal")) //big petals on plant
                {
                    Tickle();

                    if (states.plantState == 1) //if after this tickle the plant state is the new state
                    {
                        HideOrShowChildrenWithTag(GameObject.FindGameObjectWithTag("Plant").transform, "Spikes", false); //hide all the spikes on the plant
                        HideOrShowChildrenWithTag(GameObject.FindGameObjectWithTag("Plant").transform, "Blossom", true); //show all the blossoms on the plant
                        HideOrShowChildrenWithTag(GameObject.FindGameObjectWithTag("Plant").transform, "SmallBlossom", true); //show all the small blossoms on the plant
                        GameObject.FindGameObjectWithTag("PoisonParticlesExplorer").GetComponent<ParticleSystem>().Stop(); //stop the plant from emitting poison
                    }
                }
                else if (hit.collider.transform.CompareTag("Blossom")) //the blossoms underneath the spikes
                {
                    TapBlossoms(hit.collider.transform);
                }
                else if (hit.collider.CompareTag("Seed"))
                {
                    Destroy(hit.collider.gameObject); //remove the seed
                    states.plantState = 5; //tell the game that the user ate the seed and wins
                }
            }
        }
    }

    //Place Plant intantiates the desired object and tells the state handler about it
    private void PlacePlant(GameObject objectToPlace)
    {
        Instantiate(objectToPlace, states.placementPose.position, states.placementPose.rotation);
        GameObject.FindGameObjectWithTag("Plant").transform.LookAt(Camera.main.transform);
        states.placed = true;
    }

    //Tickle checks for input on the petal, changes its colour and for if the user has tapped enough times
    private void Tickle()
    {
        switch (tickleCount) //depending on the amount the object has been tickled
        {
            case 1: petalMat.color = color1; tickleCount++; break; //set the materials color and increase the tickle count
            case 2: petalMat.color = color2; tickleCount++; break;
            case 3: petalMat.color = color3; tickleCount++; break;
            case 4: petalMat.color = color4; tickleCount++; break;
            case 5: petalMat.color = goal; states.plantState = 1; tickleCount = 0; break; //set the materials colour to the goal colour, change the plant state and stop the tickle count
            default: break;
        }
    }

    //Destroy Children With Tag searches for all the children with a specific tag and then hides them
    private void HideOrShowChildrenWithTag(Transform parent, string tag, bool show)
    {
        GameObject[] children = new GameObject[parent.childCount]; //a game object array with the length of the amount of children in this game object
        for (int i = 0; i < parent.childCount; i++) //fill the game object array with all the children in this game object
        {
            children[i] = parent.GetChild(i).gameObject;
        }
        foreach (GameObject obj in children) //go through all the game objects children
        {
            if (obj.CompareTag(tag))
            {
                obj.SetActive(show); //and either hides or displays the ones with this tag
            }
        }
    }

    //Tap Blossoms checks for the right order for tapping the blossoms and activates the next state if the user tapped right
    private void TapBlossoms(Transform blossom)
    {
        switch (states.blossomCount)
        {
            case 1:
                if (blossom.name == blossom1)
                {
                    blossom.GetComponent<Renderer>().material = newMat; //change the blossoms material to the new one
                    states.blossomCount = 2; //only increase the counter if the blossom was the right one
                }
                else
                {
                    //set all blossoms back to the old material
                    GameObject[] blossoms = GameObject.FindGameObjectsWithTag("Blossom");
                    for (int i = 0; i < blossoms.Length; i++)
                    {
                        blossoms[i].GetComponent<Renderer>().material = oldMat;
                    }

                    states.blossomCount = 1; //reset the counter
                }
                break;
            case 2:
                if (blossom.name == blossom2)
                {
                    blossom.GetComponent<Renderer>().material = newMat;
                    states.blossomCount = 3;
                }
                else
                {
                    //set all blossoms back to the old material
                    GameObject[] blossoms = GameObject.FindGameObjectsWithTag("Blossom");
                    for (int i = 0; i < blossoms.Length; i++)
                    {
                        blossoms[i].GetComponent<Renderer>().material = oldMat;
                    }

                    states.blossomCount = 1; //reset the counter
                }
                break;
            case 3:
                if (blossom.name == blossom3)
                {
                    blossom.GetComponent<Renderer>().material = newMat;
                    states.blossomCount = 4;
                }
                else
                {
                    //set all blossoms back to the old material
                    GameObject[] blossoms = GameObject.FindGameObjectsWithTag("Blossom");
                    for (int i = 0; i < blossoms.Length; i++)
                    {
                        blossoms[i].GetComponent<Renderer>().material = oldMat;
                    }

                    states.blossomCount = 1; //reset the counter
                }
                break;
            case 4:
                if (blossom.name == blossom4)
                {
                    blossom.GetComponent<Renderer>().material = newMat;
                    states.plantState = 2;
                    states.blossomCount = 0;
                }
                else
                {
                    //set all blossoms back to the old material
                    GameObject[] blossoms = GameObject.FindGameObjectsWithTag("Blossom");
                    for (int i = 0; i < blossoms.Length; i++)
                    {
                        blossoms[i].GetComponent<Renderer>().material = oldMat;
                    }

                    states.blossomCount = 1; //reset the counter
                }
                break;
            default: break;
        }
    }
}
