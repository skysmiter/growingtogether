//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//Lisa Fr�hlich Gabra, #766765, Project 5: Expanding the Garden of Knowledge  //
//Growing TogethAR                                                            //
//Script to handle the collection of the seed                                 //
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


//last changes: 11.02.2022


//What it do:
// - instantiates the seed in the moment the dropping animation is over


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seed : MonoBehaviour
{
    private StateHandler states; //ref to the script that stores the riddles states

    [SerializeField] AnimationClip dropSeed;

    // Start is called before the first frame update
    void Start()
    {
        states = GetComponent<StateHandler>(); //get the script from the script handler
    }

    // Update is called once per frame
    void Update()
    {
        if (states.plantState == 2) //if the blossoms have been tapped
        {
            StartCoroutine(WaitForAnim(dropSeed, 3)); //wait for the dropping animation to end
        }
        else if (states.plantState == 3 && GameObject.FindGameObjectWithTag("Seed") == null) //if the dropping animation is over and the seed has not been placed yet
        {
            PlantSeed(); //place the seed
        }
    }

    //Plant Seed instantiates the new seed model at the position where the old model fell on the floor
    private void PlantSeed()
    {
        GameObject seed = GameObject.FindGameObjectWithTag("SeedBone");
        Transform seedTrans = seed.transform;
        Destroy(seed);
        Instantiate(states.seed, seedTrans.position, seedTrans.rotation);
        states.plantState = 4; //tell the game that the seed has been dropped
    }


    //Thanks Torben
    private IEnumerator WaitForAnim(AnimationClip anim, int state)
    {
        yield return new WaitForSeconds(anim.length);
        states.plantState = state;
    }
}
