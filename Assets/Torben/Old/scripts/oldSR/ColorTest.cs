using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTest : MonoBehaviour
{

    Color32 startColor = new Color32(168, 50, 50, 255);
    Color32 goalColor = new Color32(64, 50, 168, 255);
    Color32 currentColor;
    bool goalreached;


    private void Start()
    {
        startColor = new Color32(168, 50, 50, 255);
        goalColor = new Color32(64, 50, 168, 255);
        gameObject.GetComponent<Renderer>().material.color = startColor;
    }

    void Update()
    {
        if (gameObject.GetComponent<Renderer>().material.color.r <= goalColor.r/255) //does not work ^^
        {
            goalreached = true;
        }
        

        if (!goalreached)
        {
           
            gameObject.GetComponent<Renderer>().material.color = Color32.Lerp(gameObject.GetComponent<Renderer>().material.color, startColor, Time.deltaTime * 1f);
            currentColor = gameObject.GetComponent<Renderer>().material.color;
            ClickToColorChange();
        }


    }


    void ClickToColorChange()
    {
        #region Raycast - Clicking on collider obj
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    if (hitObject.transform.gameObject == this.gameObject)
                    {
                        //color = currentColor;
                        currentColor.r -= 21;
                        currentColor.g += 0;
                        currentColor.b += 23;
                        
                        gameObject.GetComponent<Renderer>().material.color = currentColor;

                    }
                    else return;
                }
                else return;
            }
            else return;
        }
        else return;
        #endregion
    }

}
