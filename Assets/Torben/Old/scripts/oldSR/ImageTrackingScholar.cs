using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

[RequireComponent(typeof(ARTrackedImageManager))]
public class ImageTrackingScholar : MonoBehaviour
{
    private ARTrackedImageManager arTrackedImageManager;

    //object for the scholar book
    [SerializeField] private GameObject scholarBookPrefab;
    GameObject spawnObject;

    //test    
    [SerializeField] private GameObject testPrefab;
    GameObject testSpawnObject;

    //connection to the NecktarImageBehaviour script (its on the button)
   // [SerializeField] private NecktarImageBehaviour necktarImageBehaviour;

    //Indicator
    [SerializeField] private GameObject scanIndicatorImage;

    private void Awake()
    {
       // if (!necktarImageBehaviour) FindObjectOfType<NecktarImageBehaviour>();
        arTrackedImageManager = gameObject.GetComponent<ARTrackedImageManager>();

        spawnObject = Instantiate(scholarBookPrefab, Vector3.zero, Quaternion.identity);
        spawnObject.SetActive(false);
        
        scanIndicatorImage.SetActive(true); //show image from beginning

        testSpawnObject = Instantiate(testPrefab, Vector3.zero, Quaternion.identity);
        testSpawnObject.SetActive(false);
    }

    private void OnEnable()
    {
        arTrackedImageManager.trackedImagesChanged += OnImageChanged;
    }
    private void OnDisable()
    {
        arTrackedImageManager.trackedImagesChanged -= OnImageChanged;
    }

    void OnImageChanged(ARTrackedImagesChangedEventArgs args) //what happens if we track an image
    {

        foreach (ARTrackedImage trackedImage in args.added)
        {
            OnUpdateImage(trackedImage);
        }
        foreach (var trackedImage in args.updated)
        {
            OnUpdateImage(trackedImage);
        }
    }

    void OnUpdateImage(ARTrackedImage trackedImage) //what should happen on/with the tracked image
    {
        if (scanIndicatorImage.activeSelf) scanIndicatorImage.SetActive(false);

        //vv still needs work, does not work atm (book gets shown but not the testobject/ui button)
        //maybe because there is a problem with more then one image being tracked
        if (/*!necktarImageBehaviour.necktarImageScanned &&*/ trackedImage.name == "SeedImage" /*necktarImageBehaviour.necktarImageName*/)
        {
            if ((testPrefab != null))
            {
                Vector3 position = trackedImage.transform.position;
                Quaternion rotation = trackedImage.transform.rotation;
                testSpawnObject.transform.position = position;
                testSpawnObject.transform.rotation = rotation;
                testSpawnObject.SetActive(true);
            }

           // necktarImageBehaviour.NecktarImageScanned();
            //necktarImageBehaviour.necktarImageScanned = true;
        }
       // else if (trackedImage.name != necktarImageBehaviour.necktarImageName)
        {
            if ((scholarBookPrefab != null))
            {
                Vector3 position = trackedImage.transform.position;
                Quaternion rotation = trackedImage.transform.rotation;
                spawnObject.transform.position = position;
                spawnObject.transform.rotation = rotation;
                spawnObject.SetActive(true);
            }
        }
    }
}
