using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

[RequireComponent(typeof(ARTrackedImageManager))]
public class ImageTrackingScholarTwo : MonoBehaviour
{
    private ARTrackedImageManager arTrackedImageManager;

    //object for the scholar book
    [SerializeField] private GameObject scholarBookPrefab;
    GameObject spawnObject;



    //test    
    [SerializeField] private GameObject testPrefab;
    GameObject testSpawnObject;



    //connection to the NecktarImageBehaviour script (its on the button)
    //[SerializeField] private NecktarImageBehaviour necktarImageBehaviour;

    //Indicator
    [SerializeField] private GameObject scanIndicatorImage;

    private void Awake()
    {
       // if (!necktarImageBehaviour) FindObjectOfType<NecktarImageBehaviour>();
        arTrackedImageManager = gameObject.GetComponent<ARTrackedImageManager>();

        spawnObject = Instantiate(scholarBookPrefab, Vector3.zero, Quaternion.identity);
        spawnObject.SetActive(false);

        scanIndicatorImage.SetActive(true); //show image from beginning


        //test
        testSpawnObject = Instantiate(testPrefab, Vector3.zero, Quaternion.identity);
        testSpawnObject.SetActive(false);
    }

    private void OnEnable()
    {
        arTrackedImageManager.trackedImagesChanged += OnImageChanged;
    }
    private void OnDisable()
    {
        arTrackedImageManager.trackedImagesChanged -= OnImageChanged;
    }

    void OnImageChanged(ARTrackedImagesChangedEventArgs args) //what happens if we track an image
    {

        foreach (ARTrackedImage trackedImage in args.added)
        {
            ActivateTrackedObject(trackedImage);
        }
        foreach (var trackedImage in args.updated)
        {
            UpdateTrackedObject(trackedImage);
        }
        foreach (var trackedImage in args.removed)
        {
            if (trackedImage.referenceImage.name != "SeedImage" /*necktarImageBehaviour.necktarImageName*/)
            {
                Destroy(spawnObject);
            }
        }
    }


    void ActivateTrackedObject(ARTrackedImage trackedImage)
    {
        if (scanIndicatorImage.activeSelf) scanIndicatorImage.SetActive(false);
        if (trackedImage.referenceImage.name == "SeedImage"/*necktarImageBehaviour.necktarImageName*/)//handles the reward-seed image -> should only happen once in the start of scanning THE specific image
        {
            //just to test
            Vector3 position = trackedImage.transform.position;
            Quaternion rotation = trackedImage.transform.rotation;
            testSpawnObject.transform.position = position;
            testSpawnObject.transform.rotation = rotation;
            testSpawnObject.SetActive(true);
            //just to test


            //necktarImageBehaviour.NecktarImageScanned();
        }
        else if (trackedImage.referenceImage.name != "SeedImage" /*necktarImageBehaviour.necktarImageName*/) //handles the scholar-book image
        {
            if ((scholarBookPrefab != null))
            {
                Vector3 position = trackedImage.transform.position;
                Quaternion rotation = trackedImage.transform.rotation;
                spawnObject.transform.position = position;
                spawnObject.transform.rotation = rotation;
                spawnObject.SetActive(true);
            }
        }
    }
    void UpdateTrackedObject(ARTrackedImage trackedImage) //what should happen on/with the tracked image
    {
        if (scanIndicatorImage.activeSelf) scanIndicatorImage.SetActive(false);

        //if (trackedImage.referenceImage.name == necktarImageBehaviour.necktarImageName)
        //{
        //    if ((testPrefab != null))
        //    {
        //        Vector3 position = trackedImage.transform.position;
        //        Quaternion rotation = trackedImage.transform.rotation;
        //        testSpawnObject.transform.position = position;
        //        testSpawnObject.transform.rotation = rotation;
        //        testSpawnObject.SetActive(true);
        //    }

        //    necktarImageBehaviour.NecktarImageScanned();
        //    //necktarImageBehaviour.necktarImageScanned = true;
        //}


        if (trackedImage.referenceImage.name == "SeedImage"/*necktarImageBehaviour.necktarImageName*/)//handles the reward-seed image -> should only happen once in the start of scanning THE specific image
        {
            //just to test
            Vector3 position = trackedImage.transform.position;
            Quaternion rotation = trackedImage.transform.rotation;
            testSpawnObject.transform.position = position;
            testSpawnObject.transform.rotation = rotation;
            testSpawnObject.SetActive(true);
            //just to test


            //necktarImageBehaviour.NecktarImageScanned();
        }
        else if (trackedImage.referenceImage.name != "SeedImage"/*necktarImageBehaviour.necktarImageName*/)
        {
            if (trackedImage.trackingState == TrackingState.Tracking)
            {
                if ((scholarBookPrefab != null))
                {
                    Vector3 position = trackedImage.transform.position;
                    Quaternion rotation = trackedImage.transform.rotation;
                    spawnObject.transform.position = position;
                    spawnObject.transform.rotation = rotation;
                    spawnObject.SetActive(true);
                }
            }
            else
            {
                spawnObject.SetActive(false);//could make problems later on if we deactivate scripts/components through deactivating the obj
                //the specific ui-scanindicator must be manually deactivated in the necktarImagebehaviour script
            }
        }






        ////vv still needs work, does not work atm (book gets shown but not the testobject/ui button)
        ////maybe because there is a problem with more then one image being tracked
        //if (/*!necktarImageBehaviour.necktarImageScanned &&*/ trackedImage.name == "SeedImage" /*necktarImageBehaviour.necktarImageName*/)
        //{
        //    if ((testPrefab != null))
        //    {
        //        Vector3 position = trackedImage.transform.position;
        //        Quaternion rotation = trackedImage.transform.rotation;
        //        testSpawnObject.transform.position = position;
        //        testSpawnObject.transform.rotation = rotation;
        //        testSpawnObject.SetActive(true);
        //    }

        //    necktarImageBehaviour.NecktarImageScanned();
        //    //necktarImageBehaviour.necktarImageScanned = true;
        //}
        //else if (trackedImage.name != necktarImageBehaviour.necktarImageName)
        //{
        //    if ((scholarBookPrefab != null))
        //    {
        //        Vector3 position = trackedImage.transform.position;
        //        Quaternion rotation = trackedImage.transform.rotation;
        //        spawnObject.transform.position = position;
        //        spawnObject.transform.rotation = rotation;
        //        spawnObject.SetActive(true);
        //    }
        //}
    }
}
