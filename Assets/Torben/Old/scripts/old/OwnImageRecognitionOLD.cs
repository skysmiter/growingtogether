//by Torben - image tracking (instantiate obj)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;


[RequireComponent(typeof(ARTrackedImageManager))] //needus meedus this thing alla
public class OwnImageRecognitionOLD : MonoBehaviour
{


    private ARTrackedImageManager arTrackedImageManager;

    [SerializeField] private GameObject[] prefabs;
    private Dictionary<string, GameObject> spawnedPrefabs = new Dictionary<string, GameObject>(); //set private later
    //IMPORTANT: the names of all objetcs in the prefabs array need to be the exact same as the image name in the imageLibary we want them to be tracked on
    //prefab name MUST BE referenceImage name

    [SerializeField] private GameObject scanIndicatorImage;

    //for the ending Image
    //[SerializeField] private string necktarImage;
    //bool necktarImageScanned;



    private void Awake()
    {
        arTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();

        //newImageObject = Instantiate(prefabs, Vector3.zero, Quaternion.identity); //spawns it (in beginning only once)
        //newImageObject.SetActive(false);

        foreach (var prefab in prefabs)
        {
            GameObject newPrefab = Instantiate(prefab, Vector3.zero, Quaternion.identity); //default pos&rot into new GameObject
            newPrefab.name = prefab.name; //get the name as a reference point aside the obj
            spawnedPrefabs.Add(prefab.name, newPrefab); //add the object & its name together into the dictionary
            newPrefab.SetActive(false);
        }
        scanIndicatorImage.SetActive(true); //show image from beginning
    }

    private void OnEnable()
    {
        arTrackedImageManager.trackedImagesChanged += OnImageChanged;
    }
    private void OnDisable()
    {
        arTrackedImageManager.trackedImagesChanged -= OnImageChanged;
    }

    void OnImageChanged(ARTrackedImagesChangedEventArgs args) //what happens if we track an image
    {

        foreach (ARTrackedImage trackedImage in args.added)
        {
            if (trackedImage.trackingState != TrackingState.None)
            {
                //if (trackedImage.name == necktarImage && !necktarImageScanned)
                //{

                //    necktarImageScanned = true;
                //}
                //else
                    OnUpdateImage(trackedImage);

            }
            else
            {
                spawnedPrefabs[trackedImage.name].SetActive(false); //kinda does not work lul
            }
        }
        foreach (var trackedImage in args.updated)
        {
            if (trackedImage.trackingState != TrackingState.None)
            {
                OnUpdateImage(trackedImage);
            }
            else
            {
                //scanIndicatorImage.SetActive(true); //TEST
                spawnedPrefabs[trackedImage.name].SetActive(false); //kinda does not work lul
            }

        }

        //does not work on android
        foreach (var trackedImage in args.removed) //lul only works with arkit not arcore - rip android
        {
            spawnedPrefabs[trackedImage.name].SetActive(false); //finds the current trackedimageName and disables the gameobj with that name in the dictionary
        }
        //does not work on android

    }

    void OnUpdateImage(ARTrackedImage trackedImage) //what should happen on/with the tracked image
    {
        if (prefabs != null)
        {
            if (scanIndicatorImage.activeSelf) scanIndicatorImage.SetActive(false);

            string name = trackedImage.referenceImage.name;
            Vector3 position = trackedImage.transform.position;
            Quaternion rotation = trackedImage.transform.rotation;

            GameObject prefab = spawnedPrefabs[name];
            prefab.transform.position = position;
            prefab.transform.rotation = rotation;
            prefab.SetActive(true);

            //foreach (var obj in spawnedPrefabs.Values)
            //{
            //    if (obj.name != name)
            //    {
            //        //obj.SetActive(false); //deactivates obj if image is a different one (only one object at a time) -> problem: it always stops displaying the prefabs after the second one in the dictionary
            //    }
            //}
        }
        //if (prefabs != null)
        //{

        //    newImageObject.transform.SetPositionAndRotation(trackedImage.transform.position, trackedImage.transform.rotation);
        //    if (!newImageObject.activeSelf) newImageObject.SetActive(true);
        //}
    }


}
