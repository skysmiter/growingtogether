//DOES NOT WORK: MAYBE BECAUSE OF INSTANTIATION OR SOMETHING IN THE CODE IS NOT CORRECT
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
//DOES NOT WORK: MAYBE BECAUSE OF INSTANTIATION OR SOMETHING IN THE CODE IS NOT CORRECT

//public class ShowInfo : MonoBehaviour
//{
//    List<InfoBehavior> infos = new List<InfoBehavior>();
//    Vector3[] defaultScales;


//    private void Start()
//    {
//        infos = FindObjectsOfType<InfoBehavior>().ToList();
//        //infos = GetComponentsInChildren<InfoBehavior>().ToList();

//        defaultScales = new Vector3[infos.Count];
//        for (int i = 0; i < infos.Count; i++)
//        {
//            defaultScales[i] = infos[i].gameObject.transform.localScale;
//        }
//    }
//    private void Update()
//    {
//        if (Physics.Raycast(Camera.current.transform.position, transform.forward, out RaycastHit hit))
//        {
//            GameObject go = hit.collider.gameObject;
//            if (go.CompareTag("InfoObject"))
//            {
//                //IncreaseSize(go, 1);
//                OpenInfo(go.GetComponent<InfoBehavior>());
//            }
//        }
//        else
//        {
//            CloseAllInfo();
//        }
//    }

//    void IncreaseSize(GameObject scaleObject, int number)
//    {
//        for (int i = 0; i < defaultScales.Length; i++)
//        {
//            if (number == 1)
//            {
//                if (scaleObject.transform.localScale == defaultScales[i])
//                {
//                    scaleObject.transform.localScale = Vector3.Lerp(scaleObject.transform.localScale, defaultScales[i] * 2, Time.deltaTime * 6);
//                }
//            }
//            else if (number == 0)
//            {
//                if (scaleObject.transform.localScale == defaultScales[i] * 2)
//                {
//                    scaleObject.transform.localScale = Vector3.Lerp(scaleObject.transform.localScale, defaultScales[i] / 2, Time.deltaTime * 6);
//                }
//            }

//        }



//    }

//    void OpenInfo(InfoBehavior desiredInfo)
//    {
//        foreach (var info in infos)
//        {
//            if (info == desiredInfo)
//            {
//                info.OpenInfo();
//            }
//            else
//            {
//                info.CloseInfo();
//            }
//        }
//    }
//    void CloseAllInfo()
//    {
//        foreach (InfoBehavior info in infos)
//        {
//            info.CloseInfo();
//            //IncreaseSize(info.gameObject, 0);
//        }
//    }
//}
public class ShowInfo : MonoBehaviour
{
    List<InfoBehavior> infos = new List<InfoBehavior>();
    Camera arCamera;


    private void Start()
    {
        arCamera = Camera.main;
        infos = GetComponentsInChildren<InfoBehavior>().ToList();
    }
    private void Update()
    {
        var screenCenter = arCamera.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));
        RaycastHit hit;
        if (Physics.Raycast(screenCenter, transform.forward, out hit))
        {
            //GameObject go = hit.collider.gameObject;
            if (hit.transform.CompareTag("InfoObject"))
            {
                OpenInfo(hit.transform.gameObject.GetComponent<InfoBehavior>());
            }
        }
        else
        {
            CloseAllInfo();
        }
    }

    void OpenInfo(InfoBehavior desiredInfo)
    {
        foreach (var info in infos)
        {
            if (info == desiredInfo)
            {
                info.OpenInfo();
            }
            else
            {
                info.CloseInfo();
            }
        }
    }
    void CloseAllInfo()
    {
        foreach (InfoBehavior info in infos)
        {
            info.CloseInfo();
        }
    }
}
