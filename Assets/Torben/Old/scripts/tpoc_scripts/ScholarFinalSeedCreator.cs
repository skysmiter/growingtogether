//by torben - activates the seed object && allows clicking on necktar & seed to activate the NecktarImageBehaviour script further
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//connected to NecktarImageBehaviour through its public bools
//script is on same object as NecktarImageBehaviour
//script needs a gameobject with the tag "ScholarFinalSeed" -> its inside the PoisonPlant Prefab, ScholarRiddle
public class ScholarFinalSeedCreator : MonoBehaviour
{
    GameObject scholarFinalSeed;
    bool gameObjectConnected;
    float speed = 3f;
    [HideInInspector] public bool necktarDrunk;   //both used in NecktarImageBehaviour
    [HideInInspector] public bool seedPlantable;  //both used in NecktarImageBehaviour

    private void Start()
    {
        scholarFinalSeed = null;
    }
    private void Update()
    {
        if (!scholarFinalSeed) //connect the obj
        {
            scholarFinalSeed = GameObject.FindGameObjectWithTag("ScholarFinalSeed");
            return;
        }
        else if (!gameObjectConnected)
        {
            scholarFinalSeed.transform.localScale = Vector3.zero;
            scholarFinalSeed.SetActive(false);
            gameObjectConnected = true;
        }


        if (scholarFinalSeed.activeSelf && !seedPlantable)
        {
            ClickOnSeed();
        }
    }

    void ClickOnSeed()
    {
        #region Raycast - Clicking on collider obj
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    if (hitObject.transform.gameObject == scholarFinalSeed)
                    {
                        if (scholarFinalSeed.transform.childCount >= 2)
                        {
                            if (scholarFinalSeed.transform.GetChild(0).gameObject.activeSelf)
                            {
                                scholarFinalSeed.transform.GetChild(0).gameObject.SetActive(false);
                                scholarFinalSeed.transform.GetChild(1).gameObject.SetActive(true);
                                necktarDrunk = true;
                            }
                            else
                            {
                                scholarFinalSeed.transform.GetChild(1).gameObject.SetActive(false);
                                seedPlantable = true; //show the button
                            }
                        }

                    }
                    else return;
                }
                else return;
            }
            else return;
        }
        else return;
        #endregion
    }

    public void CreateTheSeed()
    {
        if (scholarFinalSeed)
        {
            scholarFinalSeed.SetActive(true);
            StartCoroutine(MyCoroutine());
        }
    }
    IEnumerator MyCoroutine()
    {
        yield return new WaitForSeconds(4f);
        while (scholarFinalSeed.transform.localScale.x <= 0.95f)
        {
            scholarFinalSeed.transform.localScale = Vector3.Lerp(scholarFinalSeed.transform.localScale, Vector3.one, Time.deltaTime * speed);
            yield return null;
        }
    }

}
