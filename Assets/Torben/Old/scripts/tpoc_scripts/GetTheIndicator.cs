//by torben - clicking at the scan-indicator-hologram-object on the book will activate an UI image 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//connected to the same obj, as the NecktarImagebehaviour
//we call the hologramScanIndicatorObj (tag = "ImageScanIndicatorObj") from the PoisonPlant prefab (child to the ScholarRiddle obj)
public class GetTheIndicator : MonoBehaviour
{
    GameObject hologramScanIndicatorObj; //the hologram object that will be clicked (on the book)
    [SerializeField] GameObject scanIndicatorUIImage; //this image-obj must be inactive from the start, otherwise its shown all the time
    Camera arCamera;
    bool indicatorWasUsed;


    private void Start()
    {
        arCamera = Camera.main;
        if (!scanIndicatorUIImage) scanIndicatorUIImage = GameObject.FindGameObjectWithTag("ScanUIImage");
        scanIndicatorUIImage.SetActive(false);
    }
    private void Update() //CURRENTLY IS THE IMAGE VISIBLE ALL THE TIME & THE NECKTARIMAGEBEHAVIOR CANT FIND THIS SCRIPT BECAUSE IT GETS DEACTIVATED/IS INACTIVE FROM THE START -> NO ACCES TO THE CODE-COMPONENT
    {
        //we need to check if we already connected the obj, otherwise we need to do it if possible
        if (!hologramScanIndicatorObj)//we have it in update not in start, because the ImageScanIndicatorObj will be instantiated at a later point
        {
            hologramScanIndicatorObj = GameObject.FindGameObjectWithTag("ImageScanIndicatorObj");
            return;
        }

        if (hologramScanIndicatorObj.activeSelf)
        {
            if (!indicatorWasUsed)
            {
                #region Raycast - Looking on collider obj (not used)
                ////Raycast when looking on the collider
                //RaycastHit hit;
                //if (Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit))//if (Physics.Raycast(screenCenter, transform.forward, out hit))
                //{
                //    if (hit.transform.gameObject == gameObject)
                //    {
                //        scanIndicatorUIImage.SetActive(true);
                //        indicatorWasUsed = true; //flag to only allow scanning once
                //    }
                //    else return;
                //}
                //else return;
                #endregion

                #region Raycast - Clicking on collider obj
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began)
                    {
                        Ray ray = arCamera.ScreenPointToRay(touch.position);
                        RaycastHit hitObject;
                        if (Physics.Raycast(ray, out hitObject))
                        {
                            if (hitObject.transform.gameObject == hologramScanIndicatorObj)
                            {
                                scanIndicatorUIImage.SetActive(true);
                                indicatorWasUsed = true; //flag to only allow scanning once
                            }
                            else return;
                        }
                        else return;
                    }
                    else return;
                }
                else return;
                #endregion
            }
            else return;
        }
        else return;
    }
    public void TurnOfftheUIIndicator() //called in NecktarImageBehaviour, once the image was scanned
    {
        scanIndicatorUIImage.SetActive(false);
        indicatorWasUsed = true; //for the situation, that the user already scanned the image before the UI-image was shown 
    }
}
