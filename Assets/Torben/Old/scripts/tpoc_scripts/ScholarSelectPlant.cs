//by torben -  used to select one of the plants -> and clicking on the continue button to open next page
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScholarSelectPlant : MonoBehaviour
{
    [SerializeField] GameObject[] plantCircles;
    [SerializeField] GameObject correctPlant;

    [SerializeField] GameObject continueButton;
    [SerializeField] ScholarBookInteraction scholarBookInteraction;

    bool correctPlantOpen;
    List<Transform> gameObjectsToHide = new List<Transform>();

    private void Start()
    {
        foreach (var plantCircle in plantCircles)
        {
            plantCircle.transform.GetChild(0).localScale = Vector3.zero;
        }
        if (!scholarBookInteraction) scholarBookInteraction = GetComponentInParent<ScholarBookInteraction>();

    }
    private void Update()
    {
        ClickOnPlantCircle();
    }
    void ClickOnPlantCircle()
    {
        #region Raycast - Clicking on collider obj
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    GameObject touchedObject = hitObject.transform.gameObject;

                    #region test
                    //for (int i = 0; i < plantCircles.Length; i++)
                    //{

                    //    if (plantCircles[i] == selected)
                    //    {
                    //        //plantCircles[i].transform.GetChild(0).localScale = Vector3.one;
                    //        //Show(plantCircles[i].transform.GetChild(0), Vector3.one);

                    //        // gameObjectsToHide.Clear();
                    //        gameObjectsToHide.Remove(plantCircles[i].transform.GetChild(0));
                    //        StopCoroutine("hideTheObject");
                    //        StopCoroutine("showTheObject");
                    //        StartCoroutine(showTheObject(plantCircles[i].transform.GetChild(0)));
                    //        if (plantCircles[i] == correctPlant)
                    //        {
                    //            correctPlantOpen = true;
                    //        }
                    //        else
                    //        {
                    //            correctPlantOpen = false;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        //plantCircles[i].transform.GetChild(0).localScale = Vector3.zero;
                    //        //Show(plantCircles[i].transform.GetChild(0), Vector3.zero);
                    //        //gameObjectsToHide.Clear();
                    //        gameObjectsToHide.Add(plantCircles[i].transform.GetChild(0));
                    //        //StopCoroutine("hideTheObject");
                    //        StartCoroutine(hideTheObject(gameObjectsToHide));
                    //    }
                    //}
                    #endregion

                    if (touchedObject == plantCircles[0])
                    {
                        HideAndShow(0, 1, 2);
                    }
                    else if (touchedObject == plantCircles[1])
                    {
                        HideAndShow(1, 0, 2);
                    }
                    else if (touchedObject == plantCircles[2])
                    {
                        HideAndShow(2, 0, 1);
                    }
                    else if (touchedObject == continueButton)
                    {
                        if (correctPlantOpen)
                        {
                           // scholarBookInteraction.openSecondPage = true;
                        }
                    }
                }

            }

        }
        #endregion
    }


    void HideAndShow(int show, int hideOne, int hideTwo)
    {
        StopAllCoroutines();
        StartCoroutine(showTheObject(plantCircles[show].transform.GetChild(0)));
        gameObjectsToHide.Clear();
        gameObjectsToHide.Add(plantCircles[hideOne].transform.GetChild(0));
        gameObjectsToHide.Add(plantCircles[hideTwo].transform.GetChild(0));
        StartCoroutine(hideTheObject(gameObjectsToHide));

        if (plantCircles[show] == correctPlant) correctPlantOpen = true;
        else correctPlantOpen = false;
    }
    IEnumerator showTheObject(Transform trans)
    {
        while (true)
        {
            trans.localScale = Vector3.Lerp(trans.localScale, Vector3.one, Time.deltaTime * 6f);
            yield return null;
        }
    }
    IEnumerator hideTheObject(List<Transform> transList)
    {
        while (true)
        {
            foreach (var trans in transList)
            {
                trans.localScale = Vector3.Lerp(trans.localScale, Vector3.zero, Time.deltaTime * 10f);
                yield return null;
            }
        }
    }
}
