//by torben - handles the visibility of the necktar Image button & its usage (if we decide to remove the UI button, we can switch this into an empty go with scripts on it)
//handles everything that happens after we scanned the SeedImage
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


[RequireComponent(typeof(GetTheIndicator))]
[RequireComponent(typeof(ScholarFinalSeedCreator))]
public class NecktarImageBehaviour : MonoBehaviour
{
    GetTheIndicator getTheIndicator; //connected to the same obj
    ScholarFinalSeedCreator scholarFinalSeedCreator;
    GameObject poisonParticlesScholar;



    private void Start()
    {
        //v if we dont need the icon, we can remove this v//
        //deactivate the Button functionallity & its text
        gameObject.GetComponent<Button>().interactable = false;
        gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "";
        //^ if we dont need the icon, we can remove this ^//

        //connect it to the Hologram-Indicator-Object in the book
        getTheIndicator = GetComponent<GetTheIndicator>();
        scholarFinalSeedCreator = GetComponent<ScholarFinalSeedCreator>();
    }

    private void Update() //what happens if we drank the necktar & clicked on the seed -> opens UI button to plant it
    {
        if (!poisonParticlesScholar)
        {
            //referencing the poison particles VFX for the scholar
            poisonParticlesScholar = GameObject.FindGameObjectWithTag("PoisonParticlesScholar");
        }
        
        if (scholarFinalSeedCreator.necktarDrunk && !scholarFinalSeedCreator.seedPlantable)
        {
            gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "VICTORY THE POISON HAS BEEN DEFEATED"; //-> for tpoc

            //deactivating the posion VFX
            poisonParticlesScholar.SetActive(false);

            //POISON HAS BEEN DEFEATED
        }
        else if (scholarFinalSeedCreator.necktarDrunk && scholarFinalSeedCreator.seedPlantable)
        {
            gameObject.GetComponent<Button>().interactable = true;
            gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "PLANT THE SEED"; //-> for later
        }
    }
    public void NecktarImageScanned()
    {
        ////v if we dont need the icon, we can remove this v//
        ////activate the button & text
        //gameObject.GetComponent<Button>().interactable = true;
        //gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "plant the seed";
        ////^ if we dont need the icon, we can remove this ^//


        //hide the UI Indivator once the pland-seed-image was scanned
        getTheIndicator.TurnOfftheUIIndicator();
        scholarFinalSeedCreator.CreateTheSeed();
    }

    public void PlantSeed()
    {
        //plant the seed code - called by button press
    }
}
