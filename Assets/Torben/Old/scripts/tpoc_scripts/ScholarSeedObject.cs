//by torben - used to call NecktarImageBehaviour.NecktarImageScanned(); if the prefab this script is attached to gets instantiated
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//attached to an emptygameobject-prefab that gets instantiated as soon as the seed-image was scanned -> the prefab is in the OwnImageRecognition Array as a prefab to spawn
//when its created/instantiated, we call the NecktarImageScanned() from the NecktarImageBehaviour script
public class ScholarSeedObject : MonoBehaviour
{
    private NecktarImageBehaviour necktarImageBehaviour;
    private void Start()
    {
        necktarImageBehaviour = GameObject.FindGameObjectWithTag("NecktarSeedButton").GetComponent<NecktarImageBehaviour>();//FindObjectOfType<NecktarImageBehaviour>();

        necktarImageBehaviour.NecktarImageScanned();
    }
}
