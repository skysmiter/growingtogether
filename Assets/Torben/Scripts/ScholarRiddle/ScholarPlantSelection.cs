using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//once anim rdy: 
//1. needs a form of enabling/creating the colliders for each plant -> maybe in scholarturnpage script
//2. needs the animations connected to show/hide plants
//3. in the connected anim must be an activator for the turn page arrow-model -> maybe in scholarturnpage script
public class ScholarPlantSelection : MonoBehaviour
{
    [Header("Put the collider-objects & their models(as child(0)) in here")]
    [SerializeField] GameObject[] plantCircles;

    ScholarRiddleObjReference scholarRiddleObj;

    private void Start()
    {
        scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();

        foreach (var plantCirlce in plantCircles)
        {
            plantCirlce.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        ClickOnPlantCircle();
    }

    void ClickOnPlantCircle()
    {
        #region Raycast - Clicking on collider obj
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    GameObject touchedObject = hitObject.transform.gameObject;

                    if (touchedObject == plantCircles[0])
                    {
                        scholarRiddleObj.selectedPlant = 1;
                        HideAndShow(0, 1, 2); //-> function that starts showing/hiding the plant
                    }
                    else if (touchedObject == plantCircles[1])
                    {
                        scholarRiddleObj.selectedPlant = 2;
                        HideAndShow(1, 0, 2);
                    }
                    else if (touchedObject == plantCircles[2])
                    {
                        scholarRiddleObj.selectedPlant = 3;
                        HideAndShow(2, 0, 1);
                    }
                }

            }

        }
        #endregion
    }

    void HideAndShow(int show, int hideOne, int hideTwo)
    {
        // test if it deactivates child(0)
        plantCircles[show].transform.GetChild(0).gameObject.SetActive(true);
        plantCircles[hideOne].transform.GetChild(0).gameObject.SetActive(false);
        plantCircles[hideTwo].transform.GetChild(0).gameObject.SetActive(false);



        //ANIMATION TO OPEN PLANT (open plant "show" and close "hideOne" & "hideTwo") -> maybe do it with an bool, so the bool=true activates the animation to show the plant and bool=false to close
        // animation: idle(nothing) -> if bool true: show anim -> if bool false: close anim & back to idle/nothing
    }
}
