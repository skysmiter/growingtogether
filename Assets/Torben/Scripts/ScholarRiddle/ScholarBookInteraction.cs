//by torben - Handles the ScholarBookInteractions
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ScholarBookInteraction : MonoBehaviour
{
    private Camera arCamera;
    ScholarRiddleObjReference scholarRiddleObj;
    //public AnimationClip myAnimation;
    //test
    //public Animator sb_Page_Animator;

    private void Start()
    {
        scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();
        arCamera = Camera.main; //takes camera with MainCamera tag

    }

    private void Update()
    {
        ClickOnObjects();
    }


    private void ClickOnObjects()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = arCamera.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    GameObject hitGameObject = hitObject.transform.gameObject;

                    if (hitGameObject == gameObject)
                    {


                        //ANIMATION:    start the animation to open book
                        //sb_Page_Animator.SetTrigger("sb_openPage_1");
                        scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_1");
                        scholarRiddleObj.pageIsTurning = true;
                        StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[0], 1));



                        
                        
                        

                        //scholarRiddleObj.bookPAGECOUNTER = 1; //this must be in the ANIMATION CODE after the anim is done
                        gameObject.GetComponent<Collider>().enabled = false; //cant be clicked twice
                    }
                }
            }
        }
    }




}
