using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScholarHologramRing : MonoBehaviour
{
    ScholarRiddleObjReference scholarRiddleObj;

    [Header("lettersToClick must be same size as letterFloatPositions")]
    [SerializeField] GameObject[] lettersToClick; //needs same size as letterFloatPositions 
    [SerializeField] GameObject[] letterFloatPositions; //needs same size as lettersToClick

    [Header("Positions in the runic-circle (book)")]
    [SerializeField] GameObject[] letterBookPosiotions; //needs size 4 aka same size as letterBookPositionFilled
    bool[] letterOnFloatPosition; //needs same size as letterFloatPositions and lettersToClick
    bool[] letterBookPositionFilled; //needs size as letterBookPositions

    [Header("Numbers must be all 4 array numbers (start at 0) of the correct letterFloatPosition")]
    [SerializeField] int[] numberOfCorrectLetters; //must have same size as letterBookPositions & the numbers must be the array-slots of letterFloatPosition

    [SerializeField] GameObject rightOrder;


    private void Start()
    {
        scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();

        letterBookPositionFilled = new bool[letterBookPosiotions.Length]; //to check if something is on the book position[0-3]
        letterOnFloatPosition = new bool[letterFloatPositions.Length]; //to check if the letter[i] is on the float position[i]

        for (int i = 0; i < letterOnFloatPosition.Length; i++)
        {
            letterOnFloatPosition[i] = true; //set all letters onto the floatPos
        }

        rightOrder.SetActive(false);
    }
    private void Update()
    {
        LettersToPosition();
        if (letterBookPosiotions.Length == 4)
        {
            ClickOnLetters();
        }
        CheckLettersInBook();
    }

    void CheckLettersInBook()
    {
        if (letterBookPositionFilled[0] && letterBookPositionFilled[1] && letterBookPositionFilled[2] && letterBookPositionFilled[3])//if every book position �s filled
        {
            if (!letterOnFloatPosition[numberOfCorrectLetters[0]] && !letterOnFloatPosition[numberOfCorrectLetters[1]] && !letterOnFloatPosition[numberOfCorrectLetters[2]] && !letterOnFloatPosition[numberOfCorrectLetters[3]]) //if ALL letterOnFloatPosition of the correct Letters are false (all letters are in book)
            {
                foreach (var item in letterFloatPositions)
                {
                    item.SetActive(false);
                }

                for (int i = 0; i < lettersToClick.Length; i++)
                {
                    if(i != numberOfCorrectLetters[0] && i != numberOfCorrectLetters[1] && i != numberOfCorrectLetters[2] && i != numberOfCorrectLetters[3])
                    {
                        lettersToClick[i].SetActive(false);
                    }
                    
                }

                scholarRiddleObj.hologramRingIsCorrect = true;
                rightOrder.SetActive(true);
                //ANIMATION TO NEXT PAGE
            }
            else
            {
                //remove all letters from book
                for (int z = 0; z < letterOnFloatPosition.Length; z++)
                {
                    letterOnFloatPosition[z] = true;
                }
                //make bookPositions open again
                for (int i = 0; i < letterBookPositionFilled.Length; i++)
                {
                    letterBookPositionFilled[i] = false;
                }
            }

        }

    }

    void LettersToPosition()
    {

        for (int i = 0; i < lettersToClick.Length; i++) //set all letters onto their letterFloatPositions
        {
            if (letterOnFloatPosition[i])
            {
                //lettersToClick[i].transform.position = letterFloatPositions[i].transform.position;
                lettersToClick[i].transform.position = letterFloatPositions[i].transform.position - new Vector3(0.0f, -0.0047f, 0.0f);
            }
        }

    }

    void ClickOnLetters()
    {
        #region Raycast - Clicking on collider obj
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    GameObject touchedObject = hitObject.transform.gameObject;

                    for (int i = 0; i < lettersToClick.Length; i++)
                    {
                        if (touchedObject == lettersToClick[i])
                        {
                            letterOnFloatPosition[i] = false; //remove the clickedLetter from the letterFloatPosition

                            if (!letterBookPositionFilled[0])
                            {
                                lettersToClick[i].transform.position = letterBookPosiotions[0].transform.position;
                                letterBookPositionFilled[0] = true;
                            }
                            else if (!letterBookPositionFilled[1])
                            {
                                lettersToClick[i].transform.position = letterBookPosiotions[1].transform.position;
                                letterBookPositionFilled[1] = true;
                            }
                            else if (!letterBookPositionFilled[2])
                            {
                                lettersToClick[i].transform.position = letterBookPosiotions[2].transform.position;
                                letterBookPositionFilled[2] = true;
                            }
                            else if (!letterBookPositionFilled[3])
                            {
                                lettersToClick[i].transform.position = letterBookPosiotions[3].transform.position;
                                letterBookPositionFilled[3] = true;
                            }

                        }
                    }

                }

            }

        }
        #endregion
    }

}
