using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//still needs:
//1. The slider

public class ScholarBlossomCalibration : MonoBehaviour
{
    ScholarRiddleObjReference scholarRiddleObj;

    [Header("The array-slots must be the same (shape1 = preview1)")]
    [SerializeField] GameObject[] blossomShapeVariants;
    [SerializeField] GameObject[] blossomShapePreview;
    [SerializeField] GameObject baseShapePreview;
    [Header("The number must be the number of the correct shape/preview")]
    [SerializeField] int correctShapeNumber;

    [Header("The slider to change color")]
    [SerializeField] Slider colorChangeSlider;
    [Header("9 colors: all the 8 colors we want (1-8) & a base color (0)")]
    [SerializeField] Color[] colors;
    [Header("The correct color-number (1-8)")]
    [SerializeField] int correctColor;
    private Color colorToPreview;

    private void Start() //after the animation 
    {
        scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();
        //hide the preview in the beginning
        for (int i = 0; i < blossomShapePreview.Length; i++)
        {
            blossomShapePreview[i].SetActive(false);
        }
        baseShapePreview.SetActive(true);
    }
    private void Update()
    {
        if (blossomShapeVariants.Length == 4 && blossomShapePreview.Length == blossomShapeVariants.Length)
        {
            ClickOnObjects();
        }
        ColorChanger();
        foreach (var blossomPreview in blossomShapePreview)
        {
            blossomPreview.GetComponent<MeshRenderer>().material.color = colorToPreview;
        }
        baseShapePreview.GetComponent<MeshRenderer>().material.color = colorToPreview;

        //if everything is selected in the correct way
        if (blossomShapePreview[correctShapeNumber - 1].activeSelf && colorToPreview == colors[correctColor]) //&& correct color
        {
            scholarRiddleObj.blossomIsCorrect = true;
            //we can click the button
        }
        else
        {
            scholarRiddleObj.blossomIsCorrect = false;
        }
    }

    private void ColorChanger()
    {
        switch (colorChangeSlider.value)
        {
            case 0:
                colorToPreview = colors[0];
                break;
            case 1:
                colorToPreview = colors[1];
                break;
            case 2:
                colorToPreview = colors[2];
                break;
            case 3:
                colorToPreview = colors[3];
                break;
            case 4:
                colorToPreview = colors[4];
                break;
            case 5:
                colorToPreview = colors[5];
                break;
            case 6:
                colorToPreview = colors[6];
                break;
            case 7:
                colorToPreview = colors[7];
                break;
            case 8:
                colorToPreview = colors[8];
                break;

            default:
                break;
        }
    }

    private void ClickOnObjects()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    GameObject hitGameObject = hitObject.transform.gameObject;

                    //if its the first shape, show the shape0 preview
                    if (hitGameObject == blossomShapeVariants[0])
                    {
                        ChangeBlossomPreview(0);
                    }
                    //if its the second shape, show the shape1 preview
                    else if (hitGameObject == blossomShapeVariants[1])
                    {
                        ChangeBlossomPreview(1);
                    }
                    //if its the third shape, show the shape2 preview
                    else if (hitGameObject == blossomShapeVariants[2])
                    {
                        ChangeBlossomPreview(2);
                    }
                    //if its the fourth shape, show the shape3 preview
                    else if (hitGameObject == blossomShapeVariants[3])
                    {
                        ChangeBlossomPreview(3);
                    }
                }
            }
        }
    }
    private void ChangeBlossomPreview(int selected) //activates the pressed shape as preview & deactivates the remaining preview options
    {
        blossomShapePreview[selected].SetActive(true);
        for (int i = 0; i < blossomShapePreview.Length; i++)
        {
            if (i != selected)
            {
                blossomShapePreview[i].SetActive(false);
            }
        }
        if (baseShapePreview.activeSelf){
            baseShapePreview.SetActive(false);
        }
    }
}
