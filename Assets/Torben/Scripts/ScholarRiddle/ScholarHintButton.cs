using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Torben.ExtensionMethods;


public class ScholarHintButton : MonoBehaviour
{
    ScholarRiddleObjReference scholarRiddleObjReference;
    float counter;
    [SerializeField] float timeToShowHint; //ex.: 5 = 5sec
    [Header("The 7 hint-image-objects")]
    [SerializeField] GameObject[] hintTexts;
    int currentPage = 99;

    private void Awake()
    {
        foreach (var item in hintTexts)
        {
            item.SetActive(false);
        }
    }
    private void Start()
    {
        counter = timeToShowHint; //set counter to sec set in  timeToShowHint
        gameObject.GetComponent<Button>().interactable = false;//hide
    }
    private void Update()
    {
        if (!scholarRiddleObjReference) scholarRiddleObjReference = FindObjectOfType<ScholarRiddleObjReference>(); //will not be there in first frame, therefore must be in update
        else //handles showing/hiding button
        {
            if (currentPage != scholarRiddleObjReference.bookPAGECOUNTER) //if we change the page, set the button inactive
            {
                gameObject.GetComponent<Button>().interactable = false;
                foreach (var item in hintTexts)
                {
                    item.SetActive(false);
                }
                currentPage = scholarRiddleObjReference.bookPAGECOUNTER; //set current page to the new page
                counter = timeToShowHint;
            }

            if (Input.touchCount > 0) //if we click on the screen, we reset the counter to start again -> only show hint if inactive
            {
                counter = timeToShowHint; //reset
            }
            counter -= 1 * Time.deltaTime; //counts down every sec 1

            if (counter <= 0) //inactive for timeToShowHint sec
            {
                gameObject.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void PressedOnHint() //will be called if we click on the button
    {
        if (!scholarRiddleObjReference) return; //if we click on the button & scholarRiddleObjReference is null, return
        if (hintTexts.Length != 7) return;

        if (hintTexts.allInactiveOfArray())
        {
            switch (scholarRiddleObjReference.bookPAGECOUNTER)//depending on the current page, we want to show a different image(hint)
            {
                case 0:
                    hintTexts[0].SetActive(true);
                    break;
                case 1:
                    hintTexts[1].SetActive(true);
                    break;
                case 2:
                    hintTexts[2].SetActive(true);
                    break;
                case 3:
                    hintTexts[3].SetActive(true);
                    break;
                case 4:
                    hintTexts[4].SetActive(true);
                    break;
                case 5:
                    hintTexts[5].SetActive(true);
                    break;
                case 6:
                    hintTexts[6].SetActive(true); //currently problem cause page6 never gets set
                    break;

                default:
                    break;
            }
        }
        else
        {
            foreach (var item in hintTexts)
            {
                item.SetActive(false);
            }
        }
    }
}
