using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScholarImageScanner : MonoBehaviour
{
    ScholarRiddleObjReference scholarRiddleObj;

    [SerializeField] GameObject imageScanObjectToClick;
    [SerializeField] GameObject showGetUI;
    [SerializeField] GameObject showCloseBook;
    private bool showGetUIObjectInvisible;

    private void Start()
    {
        scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();
    }

    private void Update()
    {
        ClickOnObjects();
        if (scholarRiddleObj.seedImageWasScanned)
        {
            showCloseBook.SetActive(true);
            showGetUI.SetActive(false);
        }
        else if (showGetUIObjectInvisible)
        {
            showGetUI.SetActive(false);
            showCloseBook.SetActive(false);
        }
        else
        {
            showGetUI.SetActive(true);
            showCloseBook.SetActive(false);
        }
    }
    private void ClickOnObjects() //CAN BE CHANGED -> REMOVE SCRIPT FROM ANIMATION AND PUT IT HERE = EASIER TO READ::: STILL NEEDS TESTING; MARKED WITH "  /* code */  "
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    GameObject hitGameObject = hitObject.transform.gameObject;
                    if (hitGameObject == imageScanObjectToClick)
                    {
                        if (!scholarRiddleObj.seedImageCanBeScanned && !scholarRiddleObj.seedImageWasScanned)
                        {
                            showGetUIObjectInvisible = true;
                            scholarRiddleObj.seedUIImageScan.SetActive(true);
                            //test -> was visible in beginning, i disabled the raw image & the child so i need to re-show them
                            scholarRiddleObj.seedUIImageScan.GetComponent<RawImage>().enabled = true; //image
                            scholarRiddleObj.seedUIImageScan.transform.GetChild(0).gameObject.SetActive(true); //eye

                            scholarRiddleObj.seedImageCanBeScanned = true;
                        }
                        if (scholarRiddleObj.seedImageWasScanned)
                        {
                            imageScanObjectToClick.SetActive(false);
                            showGetUI.SetActive(false);
                            showCloseBook.SetActive(false);



                            scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_closeBook");



                            //StartCoroutine(shitWorkPls());
                            //StartCoroutine(scholarRiddleObj.closeBookAnim(scholarRiddleObj.animationClip[5]));

                            scholarRiddleObj.pageIsTurning = true;
                            Invoke("fuzcgjkThis", 7f);
                        }
                    }
                }
            }
        }
    }

    void fuzcgjkThis()
    {
        scholarRiddleObj.bookPAGECOUNTER = 6;
        scholarRiddleObj.pageIsTurning = false;
    }
    //IEnumerator shitWorkPls()
    //{
    //    yield return new WaitForSeconds(scholarRiddleObj.animationClip[5].length);
    //    scholarRiddleObj.bookPAGECOUNTER = 6;
    //    scholarRiddleObj.pageIsTurning = false;
    //
    //    //ANIMATION TO SHOW SEED CAN BE STARTED HERE
    //
    //}
}
