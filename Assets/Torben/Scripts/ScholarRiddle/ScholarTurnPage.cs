using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Torben.ExtensionMethods;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ScholarTurnPage : MonoBehaviour
{
    ScholarRiddleObjReference scholarRiddleObj;

    bool clickedOnArrow;
    bool preventSpamClicking;

    public string message1, message2, message3;

    private void Start()
    {
        scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();

        scholarRiddleObj.poisonMessage.GetComponent<TextMeshProUGUI>().text = message1;
    }
    private void Update()
    {
        if (!scholarRiddleObj.pageIsTurning) //if the page is not in a turning animation
        {
            SetThePage();
        }
        else //if the page is in a turning animation (when it starts & when its going)
        {
            if (scholarRiddleObj.page.Length == 5)
            {
                foreach (var eachPage in scholarRiddleObj.page)
                {
                    eachPage.SetActive(false);
                }
            }
            scholarRiddleObj.leftArrow.SetActive(false);
            scholarRiddleObj.rightArrow.SetActive(false);
        }

        ClickOnObjects();
    }
    private void ClickOnObjects() //CAN BE CHANGED -> REMOVE SCRIPT FROM ANIMATION AND PUT IT HERE = EASIER TO READ::: STILL NEEDS TESTING; MARKED WITH "  /* code */  "
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    GameObject hitGameObject = hitObject.transform.gameObject;
                    if (!scholarRiddleObj.pageIsTurning && !preventSpamClicking)
                    {
                        if (hitGameObject == scholarRiddleObj.rightArrow)
                        {
                            preventSpamClicking = true;
                            int safetyChecker = 0; //only for small test
                            clickedOnArrow = true;
                            foreach (var page in scholarRiddleObj.page)
                            {
                                if (page.activeSelf && (page.GetComponent<ScholarPageContentAnimation>() != null))
                                {
                                    page.GetComponent<ScholarPageContentAnimation>().ClosePageContent();
                                    safetyChecker = 1;
                                }
                                else if (page.activeSelf && page.GetComponent<ScholarPageContentAnimation>() == null)
                                {
                                    safetyChecker = 2;
                                }
                            }
                            if (safetyChecker == 1)
                            {
                                StartCoroutine(closePageContent(1));
                            }
                            else if (safetyChecker == 2)
                            {
                                clickedOnArrow = false;
                                nextPage();
                            }
                            #region

                            //switch (scholarRiddleObj.bookPAGECOUNTER)
                            //{
                            //    case 1:
                            //        //scholarRiddleObj.bookPAGECOUNTER++; //DEL LATER

                            //        //ANIMATION: Hide the page conent



                            //        scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_2");
                            //        StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[1], 1));



                            //        scholarRiddleObj.pageIsTurning = true;


                            //        //ANIMATION: Switch opage   -->   scholarRiddleObj.bookPAGECOUNTER++;    //this must be in the ANIMATION CODE after the anim is done
                            //        break;

                            //    case 2:
                            //        //scholarRiddleObj.bookPAGECOUNTER++; //DEL LATER

                            //        //ANIMATION: Hide the page conent
                            //        scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_3");
                            //        StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[2], 1));


                            //        scholarRiddleObj.pageIsTurning = true;


                            //        //ANIMATION: Switch opage   -->   scholarRiddleObj.bookPAGECOUNTER++;    //this must be in the ANIMATION CODE after the anim is done
                            //        break;

                            //    case 3:
                            //        //scholarRiddleObj.bookPAGECOUNTER++; //DEL LATER

                            //        //ANIMATION: Hide the page conent
                            //        scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_4");
                            //        StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[3], 1));


                            //        scholarRiddleObj.pageIsTurning = true;


                            //        //ANIMATION: Switch opage   -->   scholarRiddleObj.bookPAGECOUNTER++;    //this must be in the ANIMATION CODE after the anim is done
                            //        break;

                            //    case 4:
                            //        //scholarRiddleObj.bookPAGECOUNTER++; //DEL LATER

                            //        //ANIMATION: Hide the page conent
                            //        scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_5");
                            //        StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[4], 1));


                            //        scholarRiddleObj.pageIsTurning = true;


                            //        //ANIMATION: Switch opage   -->   scholarRiddleObj.bookPAGECOUNTER++;    //this must be in the ANIMATION CODE after the anim is done
                            //        break;

                            //    default:
                            //        break;
                            //}
                            #endregion
                        }
                        else if (hitGameObject == scholarRiddleObj.leftArrow)
                        {
                            if (scholarRiddleObj.bookPAGECOUNTER == 2)
                            {
                                preventSpamClicking = true;
                                int safetyChecker = 0;
                                clickedOnArrow = true;
                                foreach (var page in scholarRiddleObj.page)
                                {
                                    if (page.activeSelf && (page.GetComponent<ScholarPageContentAnimation>() != null))
                                    {
                                        page.GetComponent<ScholarPageContentAnimation>().ClosePageContent();
                                        safetyChecker = 1;
                                    }
                                    else if (page.activeSelf && page.GetComponent<ScholarPageContentAnimation>() == null)
                                    {
                                        safetyChecker = 2;
                                    }
                                }
                                if (safetyChecker == 1)
                                {
                                    StartCoroutine(closePageContent(0));
                                }
                                else if (safetyChecker == 2)
                                {
                                    clickedOnArrow = false;
                                    previousPage();
                                }  
                            }
                        }
                    }
                }
            }
        }
    }
    IEnumerator closePageContent(int nextOrLast)
    {
        yield return new WaitForSeconds(2f);
        clickedOnArrow = false;
        if (nextOrLast == 1) nextPage();
        else if (nextOrLast == 0) previousPage();
    }
    void previousPage()
    {
        scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_backwards");
        StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[1], -1));
        scholarRiddleObj.pageIsTurning = true;
        preventSpamClicking = false;
    }
    private void nextPage()
    {
        switch (scholarRiddleObj.bookPAGECOUNTER)
        {
            case 1:
                scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_2");
                StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[1], 1));
                scholarRiddleObj.pageIsTurning = true;
                preventSpamClicking = false;
                break;

            case 2:
                scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_3");
                StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[2], 1));
                scholarRiddleObj.pageIsTurning = true;
                preventSpamClicking = false;
                break;

            case 3:
                scholarRiddleObj.poisonMessage.GetComponent<TextMeshProUGUI>().text = message2;
                scholarRiddleObj.poisonParticles.SetActive(false);
                ChromaticAberration chromaticAb;
                scholarRiddleObj.postProcessing.GetComponent<Volume>().profile.TryGet(out chromaticAb);
                chromaticAb.intensity.value = 0.5f;
                scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_4");
                StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[3], 1));
                scholarRiddleObj.pageIsTurning = true;
                preventSpamClicking = false;
                break;

            case 4:
                scholarRiddleObj.sb_Page_Animator.SetTrigger("sb_openPage_5");
                StartCoroutine(scholarRiddleObj.waitForAnim(scholarRiddleObj.animationClip[4], 1));
                scholarRiddleObj.pageIsTurning = true;
                preventSpamClicking = false;
                break;

            default:
                break;
        }
    }
    public void SetThePage()
    {
        switch (scholarRiddleObj.bookPAGECOUNTER)
        {
            case 0:
                for (int i = 0; i < scholarRiddleObj.page.Length; i++)
                {
                    scholarRiddleObj.page[i].SetActive(false);
                }
                scholarRiddleObj.leftArrow.SetActive(false);
                scholarRiddleObj.rightArrow.SetActive(false);
                break;

            case 1:
                if (scholarRiddleObj.selectedPlant != 0)
                {
                    scholarRiddleObj.leftArrow.SetActive(false);
                    scholarRiddleObj.rightArrow.SetActive(true);
                }
                else
                {
                    scholarRiddleObj.leftArrow.SetActive(false);
                    scholarRiddleObj.rightArrow.SetActive(false);
                }
                ShowPageArrayNumber(0); //ANIMATION: show page content
                break;

            case 2:
                if (scholarRiddleObj.selectedPlant == scholarRiddleObj.correctPlant)
                {
                    scholarRiddleObj.rightArrow.SetActive(true);
                }
                scholarRiddleObj.leftArrow.SetActive(true);
                ShowPageArrayNumber(1); //ANIMATION: show page content
                break;

            case 3:
                if (scholarRiddleObj.blossomIsCorrect)
                {
                    scholarRiddleObj.rightArrow.SetActive(true);
                }
                else
                {
                    scholarRiddleObj.rightArrow.SetActive(false);
                }
                ShowPageArrayNumber(2); //ANIMATION: show page content
                scholarRiddleObj.leftArrow.SetActive(false);
                break;

            case 4:
                if (scholarRiddleObj.hologramRingIsCorrect)
                {
                    scholarRiddleObj.rightArrow.SetActive(true);
                }
                else
                {
                    scholarRiddleObj.rightArrow.SetActive(false);
                }
                ShowPageArrayNumber(3); //ANIMATION: show page content               
                break;

            case 5:
                ShowPageArrayNumber(4); //ANIMATION: show page content
                scholarRiddleObj.leftArrow.SetActive(false);
                scholarRiddleObj.rightArrow.SetActive(false);
                break;

            case 6:
                scholarRiddleObj.bookPAGECOUNTER = 99;
                scholarRiddleObj.seedObject.isInteractable(true);
                scholarRiddleObj.seedObject.GetComponent<ScholarPageContentAnimation>().OpenPageContent();
                //animation
                break;

            default:
                break;
        }
    }
    void ShowPageArrayNumber(int show)
    {
        //scholarRiddleObj.page[show].SetActive(true); //aka ANIMATIONS to show the page content
        scholarRiddleObj.page[show].SetActive(true); //aka ANIMATIONS to show the page content
        if (scholarRiddleObj.page[show].GetComponent<ScholarPageContentAnimation>() != null && !clickedOnArrow)
        {
            scholarRiddleObj.page[show].GetComponent<ScholarPageContentAnimation>().OpenPageContent();
        }
        for (int i = 0; i < scholarRiddleObj.page.Length; i++)
        {
            if (i != show) scholarRiddleObj.page[i].SetActive(false);
        }
    }
}
