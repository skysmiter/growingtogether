using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Torben.ExtensionMethods;

public class ScholarRiddleObjReference : MonoBehaviour
{
    [Header("Content of each page")]
    public GameObject[] page;

    [Header("Counter of current page")]
    public int bookPAGECOUNTER = 0;

    [Header("Objects to turn the pages")]
    public GameObject rightArrow;
    public GameObject leftArrow;

    [Header("Number that tells which plant was selected")]
    public int selectedPlant;

    [Header("Correct Plant number(can be: 1, 2 or 3)")]
    public int correctPlant;

    [Header("Animator reference (Scholar Book)")]
    public Animator sb_Page_Animator;

    [Header("AnmimationClips for book-pages")]
    public AnimationClip[] animationClip;

    [Header("To see if we are in the animation or not")]
    public bool pageIsTurning;

    [Header("If the blossom is correct")]
    public bool blossomIsCorrect;

    [Header("If the hologram-ring is filled")]
    public bool hologramRingIsCorrect;

    [Header("The UI-ImageScan can be visible")]
    public bool seedImageCanBeScanned;

    [Header("The seed Image was scanned")]
    public bool seedImageWasScanned;

    [Header("UI-Image for scanning")]
    public GameObject seedUIImageScan;

    public GameObject seedObject;

    [HideInInspector] public GameObject poisonMessage;

    [HideInInspector] public GameObject postProcessing;

    public GameObject poisonParticles;

    private void Start()
    {
        if (page.Length == 5)
        {
            foreach (var eachPage in page)
            {
                eachPage.SetActive(false);
            }
        }
        bookPAGECOUNTER = 0;
        rightArrow.SetActive(false);
        leftArrow.SetActive(false);
        selectedPlant = 0;

        seedUIImageScan = GameObject.FindGameObjectWithTag("ScanUIImage");
        seedUIImageScan.SetActive(false);

        seedObject.isInteractable(false);
        poisonMessage = GameObject.FindGameObjectWithTag("PoisonMessage");
        postProcessing = GameObject.FindGameObjectWithTag("PostProcessing");
    }

    public IEnumerator waitForAnim(AnimationClip animation, int pageCounter)
    {
        yield return new WaitForSeconds(animation.length);
        bookPAGECOUNTER += pageCounter;

        if (bookPAGECOUNTER == 6)
        {
            //start spawning-seed anim
        }
        pageIsTurning = false;
    }

    public IEnumerator closeBookAnim(AnimationClip animation)
    {
        yield return new WaitForSeconds(animation.length);
        bookPAGECOUNTER = 6;
        pageIsTurning = false;

        //ANIMATION TO SHOW SEED CAN BE STARTED HERE

    }
}
