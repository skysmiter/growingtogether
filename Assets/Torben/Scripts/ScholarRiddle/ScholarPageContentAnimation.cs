using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScholarPageContentAnimation : MonoBehaviour
{ 
    Vector3 desiredScale = Vector3.zero; //default is "off"
    public float speed = 5f;



    private void Start()
    {
        gameObject.transform.localScale = Vector3.zero;
    }


    private void Update()
    {
        gameObject.transform.localScale = Vector3.Lerp(gameObject.transform.localScale, desiredScale, Time.deltaTime * speed);
    }

    public void OpenPageContent()
    {
        desiredScale = Vector3.one;
    }
    public void ClosePageContent()
    {
        desiredScale = Vector3.zero;
    }
}
