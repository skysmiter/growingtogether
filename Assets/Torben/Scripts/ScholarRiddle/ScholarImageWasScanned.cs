using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScholarImageWasScanned : MonoBehaviour
{
    ScholarRiddleObjReference scholarRiddleObj;

    private void Start()
    {
        if (!scholarRiddleObj) scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();
    }
    private void Update()
    {
        if (!scholarRiddleObj) scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();
        if (scholarRiddleObj.seedUIImageScan.activeSelf) scholarRiddleObj.seedUIImageScan.SetActive(false);
        if (!scholarRiddleObj.seedImageWasScanned) scholarRiddleObj.seedImageWasScanned = true;
    }
}
