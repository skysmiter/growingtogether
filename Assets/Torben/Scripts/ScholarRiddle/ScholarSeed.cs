using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ScholarSeed : MonoBehaviour
{
    GameObject victory;

    ScholarRiddleObjReference scholarRiddleObj;
    ScholarTurnPage scholarTurnPage;

    private void Start()
    {
        victory = GameObject.FindGameObjectWithTag("Victory");
        victory.SetActive(false);
        scholarRiddleObj = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();
        scholarTurnPage = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarTurnPage>();
    }

    void Update()
    {
        ClickOnSeed();
    }
    void ClickOnSeed()
    {
        #region Raycast - Clicking on collider obj
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    if (hitObject.transform.gameObject == gameObject)
                    {
                        victory.SetActive(true);
                        scholarRiddleObj.poisonMessage.GetComponent<TextMeshProUGUI>().text = scholarTurnPage.message3;
                        ChromaticAberration chromaticAb;
                        scholarRiddleObj.postProcessing.GetComponent<Volume>().profile.TryGet(out chromaticAb);
                        chromaticAb.intensity.value = 0.0f;
                        gameObject.SetActive(false);

                    }
                    else return;
                }
                else return;
            }
            else return;
        }
        else return;
        #endregion
    }
}
