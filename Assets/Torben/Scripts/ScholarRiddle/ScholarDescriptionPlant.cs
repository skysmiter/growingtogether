using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//later in the animation we need:
//1. avitvate this opbject/script 
//2. animation to show the correct content (important with different selectedPlant number) -> maybe in scholarTurnPage script

public class ScholarDescriptionPlant : MonoBehaviour
{
    ScholarRiddleObjReference scholarRiddleObjReference;
    [SerializeField] GameObject[] plantInformation;
    

    private void Start()
    {
        scholarRiddleObjReference = GameObject.FindGameObjectWithTag("ScholarRiddle").GetComponent<ScholarRiddleObjReference>();
        if (plantInformation.Length == 3)
        {
            plantInformation[0].SetActive(false);
            plantInformation[1].SetActive(false);
            plantInformation[2].SetActive(false);
        }
    }
    private void Update()
    {
        if (plantInformation.Length == 3)
        {
            switch (scholarRiddleObjReference.selectedPlant)
            {
                case 0:
                    break;

                case 1:
                    plantInformation[0].SetActive(true);
                    plantInformation[1].SetActive(false);
                    plantInformation[2].SetActive(false);
                    break;

                case 2:
                    plantInformation[1].SetActive(true);
                    plantInformation[0].SetActive(false);
                    plantInformation[2].SetActive(false);
                    break;

                case 3:
                    plantInformation[2].SetActive(true);
                    plantInformation[0].SetActive(false);
                    plantInformation[1].SetActive(false);
                    break;
                default:
                    break;
            }
        }
    }

}

