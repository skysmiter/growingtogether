//by torben - script for the main menu
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    //ScholarRiddleObjReference ScholarRiddleObjReference; //for testing


    //dont remove
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }






    //              //             // 
    // Remove Later // RemoveLater //
    // ____________________________//

    /*

    public void DebugStuff()//test
    {
        // SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -1);
    }

    private void Update()
    {
        //FOR TESTING IN SCHOLAR SCENE - REMOVE LATER
        if (!ScholarRiddleObjReference) ScholarRiddleObjReference = FindObjectOfType<ScholarRiddleObjReference>();
        else gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "PAGE: " + ScholarRiddleObjReference.bookPAGECOUNTER;
    }

    */
}
