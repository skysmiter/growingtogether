using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class PageSwiper : MonoBehaviour, IDragHandler, IEndDragHandler
{
    private Vector3 panelLocation;
    public float percentThreshold = 0.2f; //how far we need to drag to get to the next page (higher number = more draggin)
    public float easing = 0.5f; //how slow the page will snap
    public int totalPages = 1;
    [HideInInspector] public int currentPage = 1;

    //[Header("Used to set scale of panels for different aspect-ratios")]
    //[SerializeField] GameObject[] panels;


    void Start()
    {
        //for (int i = 0; i < panels.Length; i++)
        //{
        //    RectTransform recT = panels[i].GetComponent<RectTransform>();
        //    recT.offsetMax = new Vector2((Screen.width * i), recT.offsetMax.y);
        //    recT.offsetMin = new Vector2((Screen.width * i), recT.offsetMin.y);
        //}
        panelLocation = transform.position;
    }
    public void OnDrag(PointerEventData data) //while dragging
    {
        float difference = data.pressPosition.x - data.position.x; //distacne we draged
        transform.position = panelLocation - new Vector3(difference, 0, 0); // idle state - distance we dragged
    }
    public void OnEndDrag(PointerEventData data)//when dragging ended (snap to side)
    {
        float percentage = (data.pressPosition.x - data.position.x) / Screen.width; //either positive or negative d4epending on left or right swipe
        if (Mathf.Abs(percentage) >= percentThreshold)
        {
            Vector3 newLocation = panelLocation;
            if (percentage > 0 && currentPage < totalPages) //left direction swipe
            {
                currentPage++;
                newLocation += new Vector3(-Screen.width, 0, 0);
            }
            else if (percentage < 0 && currentPage > 1)//right deirection swipe
            {
                currentPage--;
                newLocation += new Vector3(Screen.width, 0, 0);
            }
            StartCoroutine(SmoothMove(transform.position, newLocation, easing));
            panelLocation = newLocation;

        }
        else
        {
            StartCoroutine(SmoothMove(transform.position, panelLocation, easing));
        }

    }
    IEnumerator SmoothMove(Vector3 startpos, Vector3 endpos, float seconds)
    {
        float t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startpos, endpos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }
}