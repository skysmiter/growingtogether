using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PageSwiper))]
public class DotManager : MonoBehaviour
{
    PageSwiper pageSwiper;
    [SerializeField] GameObject[] dots;

    [Header("How the dots will be highlighted")]
    public bool onlyHighlightCurrentDot;
    public bool highlightAllPreviousDots;
    public bool highlightCurrentDotAndLittleOnPreviousDots;

    [SerializeField] Color current, previous, notYet;

    //[Header("Used to set scale of panels for different aspect-ratios")]
    //[SerializeField] GameObject[] panels;

    private void Start()
    {
        pageSwiper = gameObject.GetComponent<PageSwiper>();
        for (int i = 0; i < dots.Length; i++)
        {
            dots[i].GetComponent<Image>().color = notYet;
        }


        //change aspect-ratio of screen will change panel
        //for (int i = 0; i < panels.Length; i++)
        //{
        //    RectTransform recT = panels[i].GetComponent<RectTransform>();
        //    recT.offsetMax = new Vector2((Screen.width * i), recT.offsetMax.y);
        //    recT.offsetMin = new Vector2((Screen.width * i), recT.offsetMin.y);
        //}

    }
    private void Update()
    {
        //change aspect-ratio of screen will change panel
        //for (int i = 0; i < panels.Length; i++)
        //{
        //    RectTransform recT = panels[i].GetComponent<RectTransform>();
        //    recT.offsetMax = new Vector2((Screen.width * i), recT.offsetMax.y);
        //    recT.offsetMin = new Vector2((Screen.width * i), recT.offsetMin.y);
        //}


        if (pageSwiper.currentPage - 1 >= 0 && pageSwiper.currentPage - 1 <= dots.Length - 1) //to be sure we have same size of pages & dots
        {
            int page = pageSwiper.currentPage;
            dots[page - 1].GetComponent<Image>().color = current;

            if (onlyHighlightCurrentDot) //if we only want to highlit one dot
            {
                for (int i = 0; i < dots.Length; i++)
                {
                    if (i != page-1) //deavtivate all dots that are bigger then the current-page dot
                    {
                        dots[i].GetComponent<Image>().color = notYet;
                    }
                }
            }
            if (highlightAllPreviousDots)//if we want to highlite current dot and previous page-dots 
            {
                for (int i = 0; i < dots.Length; i++)
                {
                    if (i >= page) //deavtivate all dots that are bigger then the current-page dot
                    {
                        dots[i].GetComponent<Image>().color = notYet;
                    }
                }
            }
            if (highlightCurrentDotAndLittleOnPreviousDots)
            {
                for (int i = 0; i < dots.Length; i++)
                {
                    if (i >= page) //deavtivate all dots that are bigger then the current-page dot
                    {
                        //dots[i].GetComponent<Image>().color = new Color(1, 0, 0, 1);
                        dots[i].GetComponent<Image>().color = notYet;
                    }
                    if (i < page-1)
                    {
                        //dots[i].GetComponent<Image>().color = new Color(1, 1, 0, 1);
                        dots[i].GetComponent<Image>().color = previous;
                    }
                }
            }

        }
    }
}
