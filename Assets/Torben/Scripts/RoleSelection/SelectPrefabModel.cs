//by torben - this script activates/deactivates children of an instantiated prefab, based on the scene & what should be visible in each scene (needed because image tracking works above unity-scenes)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//THE NAMES OF the objectsToShow MUST BE the same NAMES as the names OF the SCENE where they should be visible
//since the image tracking creates problems(it seems like its ignoring scene changes), i found a weird work around, by putting EVERYTHING we want to connect to image tracking into ONE prefab
//and add this script onto the parent-prefab and turn everything we dont need at a certain point inactive & the once we need active...
//this way works, but is not clean...
//i did it this way, because i found that using image tracking with the same image (even over different scenes and with different ImageReferenceLibaries)
//causes problems if you want to display a different object (so same picture as in the first scene but different object in the second scene)

//script is attached to PoisonPlant (parent prefab)
public class SelectPrefabModel : MonoBehaviour
{
    [Header("NAMES OF OBJECTS MUST BE NAME OF SCENE WHERE DISPLAYED")]
    [SerializeField] private GameObject[] objectsToShow;
    Dictionary<string, GameObject> differentPrefabs = new Dictionary<string, GameObject>();
    string nameOfScene;

    void Start()
    {
        foreach (var objectToshow in objectsToShow)
        {
            differentPrefabs.Add(objectToshow.name, objectToshow);
        }

        foreach (var gameObject in differentPrefabs.Values)
        {
            nameOfScene = gameObject.name;

            if (SceneManager.GetActiveScene().name == nameOfScene)
            {
                differentPrefabs[nameOfScene].SetActive(true);
            }
            else
            {
                differentPrefabs[nameOfScene].SetActive(false);
            }
        }

    }
}
