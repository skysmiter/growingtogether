//by torben - script is attachet to the UI button, changes the scene
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//script is connected to SelectRoleForButton script (attached to SelectRole Prefab)
public class SelectRoleScene : MonoBehaviour
{
  
    [Header("In this script, we call the scene switch depending on the name of the scene")]
    public string role;
    private void Start()
    {
        gameObject.GetComponent<Button>().interactable = false;
        gameObject.GetComponent<Image>().enabled = false;

    }
    public void ChangeTheScene()//the string in .LoadScene() must be the name of the scene we want to call
    {
        if (role != null && (role == "Explorer" || role == "Scholar"))
            SceneManager.LoadScene(role+"Riddle");
    }
}
