//by torben - sends the name of the selected object to the UI button script & enables the button
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

//this GameObject needs the Tag InfoObject
//script is connected to SelectRoleScene script (attached to SelectRoleButton in UI)
//script is connected to ClickToGetInfo script (attached to its parent: RoleSelection obj) -> from there the ShowButton function gets called
[RequireComponent(typeof(InfoBehavior))] // because in the ClickToGetInfo script (script where ShowButton and HideButton are called), the gameObjects are called by their InfoBehavior component 
public class SelectRoleForButton : MonoBehaviour
{
    [Header("This gameObject needs a name connected to the Scene-name of the riddle")]
    
    [Tooltip("No need to fill this")]
    [SerializeField]private GameObject selectButton;
    void Start()
    {
        selectButton = GameObject.FindGameObjectWithTag("SelectButton");
    }


    public void ShowButton(string thisRole)//the string thisRole must be usable to call a scene -> 'same name as scene' or you have to change something in the SelectRoleScene script
    {
        if (thisRole != null && (thisRole == "Explorer" || thisRole == "Scholar"))
        {
            selectButton.GetComponent<Button>().interactable = true; //button now usable
            selectButton.GetComponent<Image>().enabled = true;
            selectButton.GetComponentInChildren<TextMeshProUGUI>().text = "Select " + thisRole; //what the text will show
            selectButton.GetComponent<SelectRoleScene>().role = thisRole;
        }
    }
    public void HideButton() //currently not in use
    {
        selectButton.GetComponentInChildren<TextMeshProUGUI>().text = "";
        selectButton.GetComponent<SelectRoleScene>().role = "";
        selectButton.GetComponent<Button>().interactable = false;
        selectButton.GetComponent<Image>().enabled = false;
    }

}
