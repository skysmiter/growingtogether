//by torben - controlls the size of information pupups & their rotation towards the camera
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this GameObject needs the Tag InfoObject
//InfoBehaviour is called by the ClickToGetInfo script (attached to RoleSelection obj)
public class InfoBehavior : MonoBehaviour
{
    [SerializeField] private Transform sectionInfo;
    Vector3 desiredScale = Vector3.zero; //default is "off"
    float speed = 6f;

    //look at cam
    Transform cam;
    Vector3 targetAngle = Vector3.zero;

    private void Start()
    {
        sectionInfo.localScale = Vector3.zero;

        //look at cam
        cam = Camera.main.transform;
    }


    private void Update()
    {
        sectionInfo.localScale = Vector3.Lerp(sectionInfo.localScale, desiredScale, Time.deltaTime * speed);


        //look at camera
        sectionInfo.LookAt(2*sectionInfo.transform.position - cam.position); //had to reverse it
        //targetAngle = sectionInfo.localEulerAngles;
        //targetAngle.x = 0;
        //targetAngle.z = 0;
        //sectionInfo.localEulerAngles = targetAngle;
    }

    public void OpenInfo()
    {
        desiredScale = Vector3.one;
    }
    public void CloseInfo()
    {
        desiredScale = Vector3.zero;
    }
}
