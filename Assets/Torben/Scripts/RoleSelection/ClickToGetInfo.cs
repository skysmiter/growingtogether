//by torben - handels clicking on the 3d objects & opening/closing the corresponding information boxes & showing/hiding the select button
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//script is attached to the RoleSelection (obj in PoinsonPlant prefab)
//script is connected to its children trough the tag "InfoObject" by their InfoBehaviour script (attached to Explorer/Scolar objs)
//script is connected to its children trough the tag "InfoObject" by their SelectRoleForButton script (attached to Explorer/Scolar objs) -> here we call the ShowButton()
public class ClickToGetInfo : MonoBehaviour
{
    List<InfoBehavior> infos = new List<InfoBehavior>();
    //Vector3[] defaultScales;
    private Camera arCamera;
    //private Vector2 touchPosition = default;

    //private bool infoOpen;

    private void Start()
    {

        arCamera = Camera.main;

        //infos = FindObjectsOfType<InfoBehavior>().ToList();
        infos = GetComponentsInChildren<InfoBehavior>().ToList();


        //test
        //defaultScales = new Vector3[infos.Count];
        //for (int i = 0; i < infos.Count; i++)
        //{
        //    defaultScales[i] = infos[i].gameObject.transform.localScale;
        //}
    }
    private void Update()
    {
        ClickOnObjects();


        //if (Physics.Raycast(Camera.current.transform.position, transform.forward, out RaycastHit hit))
        //{
        //    GameObject go = hit.collider.gameObject;
        //    if (go.CompareTag("InfoObject"))
        //    {
        //        //IncreaseSize(go, 1);
        //        OpenInfo(go.GetComponent<InfoBehavior>());
        //    }
        //}
        //else
        //{
        //    CloseAllInfo();
        //}
    }

    private void ClickOnObjects()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            //touchPosition = touch.position;

            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = arCamera.ScreenPointToRay(touch.position);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    if (hitObject.transform.tag == "InfoObject" /*&& !infoOpen*/)
                    {
                        OpenInfo(hitObject.transform.gameObject.GetComponent<InfoBehavior>());
                        hitObject.transform.gameObject.GetComponent<SelectRoleForButton>().ShowButton(hitObject.transform.gameObject.name); //the name of the selectable object must be connected/working with to the SelectRoleForButton script (SelectRoleScene script)
                        //IncreaseSize(hitObject.transform.gameObject, 0); //test
                        //infoOpen = true;
                    }
                    ////essentailly the else if & the infoOpen, allows us to get to a point where both infos are closed (clicking on the same objects twize OR clicking on the other object will close all infos again)
                    ////if we want to have only one info open all the time (and dont need a closed state, then we can also remove the 'else if' & 'infoOpen')
                    //else if (hitObject.transform.tag == "InfoObject" && infoOpen)
                    //{
                    //    CloseAllInfo();//test
                    //    //hitObject.transform.gameObject.GetComponent<SelectRoleForButton>().HideButton();
                    //    infoOpen = false;
                    //}

                }
            }
        }
    }

    //void IncreaseSize(GameObject scaleObject, int number)
    //{
    //    for (int i = 0; i < defaultScales.Length; i++)
    //    {
    //        if (number == 1)
    //        {
    //            if (scaleObject.transform.localScale == defaultScales[i])
    //            {
    //                scaleObject.transform.localScale = Vector3.Lerp(scaleObject.transform.localScale, defaultScales[i] * 2, Time.deltaTime * 6);
    //            }
    //        }
    //        else if (number == 0)
    //        {
    //            if (scaleObject.transform.localScale == defaultScales[i] * 2)
    //            {
    //                scaleObject.transform.localScale = Vector3.Lerp(scaleObject.transform.localScale, defaultScales[i] / 2, Time.deltaTime * 6);
    //            }
    //        }

    //    }
    //}

    void OpenInfo(InfoBehavior desiredInfo)
    {
        foreach (var info in infos)
        {
            if (info == desiredInfo)
            {
                info.OpenInfo();                
            }
            else
            {
                info.CloseInfo();
                //info.transform.gameObject.GetComponent<SelectRoleForButton>().HideButton();
            }
        }
    }
    //void CloseAllInfo()
    //{
    //    foreach (InfoBehavior info in infos)
    //    {
    //        info.CloseInfo();
    //        info.gameObject.GetComponent<SelectRoleForButton>().HideButton();
    //    }
    //}
}
