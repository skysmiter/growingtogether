using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class SingletonRotationManager : MonoBehaviour
{
    public static SingletonRotationManager Instance { set; get; }


    private void Awake()
    {
        #region Singleton Pattern 
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
        #endregion
    }

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void Update()
    {

        /*

        if (SceneManager.GetActiveScene().buildIndex == 0 || SceneManager.GetActiveScene().name == "Onboarding" || SceneManager.GetActiveScene().buildIndex == 1 || SceneManager.GetActiveScene().name == "RoleSelection")
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }
        else if (SceneManager.GetActiveScene().buildIndex == 2 || SceneManager.GetActiveScene().name == "ScholarRiddle")
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }
        else
        {
            Screen.orientation = ScreenOrientation.AutoRotation;
        }

        */
    }
}
