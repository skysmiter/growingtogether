using System.Collections;
using System;
using UnityEngine;

namespace Torben
{
    namespace ExtensionMethods
    {
        public static class ExtensionMethods
        {
            public static void isInteractable(this GameObject gameObject, bool boolean)
            {
                if (gameObject.GetComponent<Collider>())
                    gameObject.GetComponent<Collider>().enabled = boolean;
                if (gameObject.GetComponent<MeshRenderer>())
                    gameObject.GetComponent<MeshRenderer>().enabled = boolean;
                if (gameObject.GetComponent<Rigidbody>())
                {
                    gameObject.GetComponent<Rigidbody>().useGravity = boolean;
                    gameObject.GetComponent<Rigidbody>().isKinematic = !boolean;
                }
            }

            public static void resetTransform(this Transform trans)
            {
                trans.position = Vector3.zero;
                trans.localRotation = Quaternion.identity;
                trans.localScale = Vector3.one;
            }

            public static bool allInactiveOfArray(this GameObject[] myArray) //returns true if all gameobjects of this array are inactive
            {
                bool response = true;
                for (int i = 0; i < myArray.Length; i++)
                {
                    if (myArray[i].activeInHierarchy)
                    {
                        response = false;
                        break;
                    }
                }
                return response;
            }


        }
    }
    namespace Test
    {
        public static class OwnTest
        {
        }
    }
}
