﻿Introduction


This readme is designed to give instructions on how to play the augmented reality (AR) app “Growing TogethAR”, which was created by us (Torben Storch, Felix Venne, Lisa Fröhlich Gabra, Daniel Gitonga Mutuma) for our fifth semester project as part of the Expanded Realities course of studies at the h_da Hochschule Darmstadt.


Requirements


In order to experience the app properly you will need to make sure the following criteria are met: 


* Two players
* Two AR-ready android-powered mobile devices
* Enough unobstructed space to serve as a playing area
* Decent enough lighting-conditions
* The app itself (the apk file can be requested from us developers)
* The image tracking playing card (can be requested from us developers)


First Steps


To start your journey within the world of “Growing TogethAR”, both players will need to scan the image tracking playing card when first running the app on their devices. Once the image tracking recognizes the correct image, both players will be prompted to select a role. It is of the greatest import that no role is selected twice and that all roles are designated to a player in the end. For example this means that player A selects the “scholar” role and player B chooses the “explorer” role. Once both players select their respective roles the experience’s main part will begin. From here on out, it is up to both players to communicate and work together to overcome the riddles presented by the app. Good luck!